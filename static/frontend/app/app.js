(function() {
	var app = angular.module('gameApp', [
		'ngRoute',
		'ngCookies',
		'ngProgress',
		'MessageCenterModule',
		'ui.bootstrap',
	]);

	app.config(['$routeProvider', '$locationProvider' , '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
		$routeProvider.
		  when('/', {
		    templateUrl: '/views/village.html',
		    controller: 'villageController'
		}).
		  when('/login', {
		    templateUrl: '/views/login.html',
		    controller: 'authController as authCtrl'
		}).
		  when('/logout', {
		    controller: 'authController'
		}).
		  when('/register', {
		    templateUrl: '/views/register.html',
		    controller: 'registerController as regCtrl'
		}).
		  when('/village', {
		    templateUrl: '/views/village.html',
		    controller: 'villageController'
		}).
		  when('/create-village', {
		    templateUrl: '/views/create-village.html',
		    controller: 'createVillageController as cvCtrl'
		}).
		  when('/building/:building_type', {
		    templateUrl: '/views/building.html',
		    controller: 'buildingsController'
		}).
		  when('/map', {
		    templateUrl: '/views/map.html',
		    controller: 'mapController'
		}).
		  when('/map/:x/:y/info', {
		    templateUrl: '/views/coordinate-info.html',
		    controller: 'coordinateController'
		}).
		  when('/reports', {
		    templateUrl: '/views/reports.html',
		    controller: 'reportsController'
		}).
		  when('/report/:id', {
		    templateUrl: '/views/report.html',
		    controller: 'reportController'
		}).
		  when('/messages', {
		    templateUrl: '/views/messages.html',
		    controller: 'messagesController'
		}).
		  when('/messages/sent', {
		    templateUrl: '/views/messages.html',
		    controller: 'messagesController'
		}).
		  when('/message/:id', {
		    templateUrl: '/views/message.html',
		    controller: 'messageController'
		}).
		  when('/player/:id', {
		    templateUrl: '/views/player.html',
		    controller: 'playerController'
		}).
		  when('/clan/:id', {
		    templateUrl: '/views/clan.html',
		    controller: 'clanController'
		}).
		  when('/forum', {
		    templateUrl: '/views/forum.html',
		    controller: 'forumController'
		}).
		  when('/forum/topic/:id', {
		    templateUrl: '/views/topic.html',
		    controller: 'topicController'
		}).
		  otherwise({
		    redirectTo: '/'
		});

		$locationProvider.html5Mode({
		  enabled: true,
		  requireBase: false
		});

	}])
	.run(function ($rootScope, $location, $cookieStore, $http, $q, $interval, ngProgress, GameService) {
		if($location.host() == '192.168.50.240'){
			$rootScope.mediaServer = 'http://192.168.50.240:8000';
			$rootScope.server = 'http://192.168.50.240:8000';
		} else {
			$rootScope.mediaServer = '';
			$rootScope.server = '';
		}
		
        $rootScope.globals = $cookieStore.get('globals') || {};
		$rootScope.server_data = {};

		GameService.init_user_data();

		$rootScope.update_timers = function() {
			var timestamp = Math.ceil(new Date().getTime() / 1000);
			if(!$rootScope.globals || !$rootScope.server_data.frontend_time){
				return;
			}
			var frontend_time = $rootScope.server_data.frontend_time;
			var diff_time = timestamp - frontend_time;
			var server_time = $rootScope.server_data.time + diff_time;

			// get info stats
			if($rootScope.globals.currentUser && $rootScope.globals.currentUser.badges && $rootScope.globals.currentUser.badges.last_update
				&& timestamp - $rootScope.globals.currentUser.badges.last_update > 30) {
				GameService.get_unread_badges().then(function() {});
			}

			if($rootScope.globals.currentVillage.timers && timestamp - $rootScope.globals.currentVillage.timers.frontend_time > 30) {
				GameService.get_village_timers($rootScope.globals.currentVillage.id).then(function() {});
			}


			// calculate resources
			if($rootScope.globals.timers && $rootScope.globals.timers.resources.synced && $rootScope.globals.currentVillage && $rootScope.globals.currentVillage.resources){
				var currentVillage = $rootScope.globals.currentVillage;
				// STONE
				var diff = timestamp - $rootScope.globals.timers.resources.stone;
				var income = Math.floor(currentVillage.resources.stone_income / 3600 * diff);
				if(income > 0){
					diff = parseInt(income * 3600 / currentVillage.resources.stone_income);
					currentVillage.resources.stone += income;
					$rootScope.globals.timers.resources.stone += diff;
				}

				// WOOD
				diff = timestamp - $rootScope.globals.timers.resources.wood;
				income = Math.floor(currentVillage.resources.wood_income / 3600 * diff);
				if(income > 0){
					diff = parseInt(income * 3600 / currentVillage.resources.wood_income);
					currentVillage.resources.wood += income;
					$rootScope.globals.timers.resources.wood += diff;
				}

				// CLAY
				diff = timestamp - $rootScope.globals.timers.resources.clay;
				income = Math.floor(currentVillage.resources.clay_income / 3600 * diff);
				if(income > 0){
					diff = parseInt(income * 3600 / currentVillage.resources.clay_income);
					currentVillage.resources.clay += income;
					$rootScope.globals.timers.resources.clay += diff;
				}
			}

			// calculate buildings upgrade & army movements
			if($rootScope.globals.currentVillage && $rootScope.globals.currentVillage.timers){
				if($rootScope.globals.currentVillage.timers.buildings_upgrades){
					for(var i in $rootScope.globals.currentVillage.timers.buildings_upgrades){
						var upgrade = $rootScope.globals.currentVillage.timers.buildings_upgrades[i];
						var cd = (upgrade.start_at + upgrade.building_time) - server_time;
						upgrade.countdown = cd;

						if(cd < -1) {
							$rootScope.globals.currentVillage.timers.buildings_upgrades.splice(i, 1);
							continue;
						}
						if(cd == -1) {
							for(var x = 0; x < $rootScope.globals.currentVillage.buildings.length; x++) {
								if($rootScope.globals.currentVillage.buildings[x].building_type == $rootScope.globals.currentVillage.timers.buildings_upgrades[i].building.building_type) {
									$rootScope.globals.currentVillage.buildings[x].stage = $rootScope.globals.currentVillage.timers.buildings_upgrades[i].building.stage;
									$rootScope.globals.currentVillage.buildings[x].upgrading = false;
								}
							}

							if($rootScope.globals.currentVillage.id){
								GameService.get_village_timers($rootScope.globals.currentVillage.id).then(function(data){ });
							}
							for(var i in $rootScope.globals.currentVillage.buildings){
								var building = $rootScope.globals.currentVillage.buildings[i]

								if(upgrade.building.building_type == building.building_type){
									building.stage = upgrade.building.stage;
									building.upgrading = false;
		            				$rootScope.$broadcast('reloadVillage', {});
								}
							}
						}
					}
				}

				if($rootScope.globals.currentVillage.timers.army_movement){
					for(var key in $rootScope.globals.currentVillage.timers.army_movement) {
						var ut = $rootScope.globals.currentVillage.timers.army_movement[key];
						ut.countdown = ut.ended_at - server_time;
						
						if(ut.countdown < 0 && $rootScope.globals.currentVillage.id){
							GameService.get_village_timers($rootScope.globals.currentVillage.id).then(function(data){ });
						}
					}
				}

				if($rootScope.globals.currentVillage.timers.market_movement){
					for(var key in $rootScope.globals.currentVillage.timers.market_movement) {
						var ut = $rootScope.globals.currentVillage.timers.market_movement[key];
						ut.countdown = ut.end_at - server_time;

						if(ut.countdown < 0 && $rootScope.globals.currentVillage.id) {
							if(ut.status == 'R') {
								if(ut.requested.resource == 'stone') {
									$rootScope.globals.currentVillage.resources.stone += ut.requested.value;
								} else if(ut.requested.resource == 'wood') {
									$rootScope.globals.currentVillage.resources.wood += ut.requested.value;
								} else if(ut.requested.resource == 'clay') {
									$rootScope.globals.currentVillage.resources.clay += ut.requested.value;
								}
							} else if(ut.status == 'D' && ut.end_village.id == $rootScope.globals.currentVillage.id) {
								if(ut.offered.resource == 'stone') {
									$rootScope.globals.currentVillage.resources.stone += ut.offered.value;
								} else if(ut.offered.resource == 'wood') {
									$rootScope.globals.currentVillage.resources.wood += ut.requested.value;
								} else if(ut.offered.resource == 'clay') {
									$rootScope.globals.currentVillage.resources.clay += ut.offered.value;
								}
							}

							if($rootScope.globals.currentVillage.resources.stone > $rootScope.globals.currentVillage.resources.storage_space) {
								$rootScope.globals.currentVillage.resources.stone = $rootScope.globals.currentVillage.resources.storage_space;
							}

							if($rootScope.globals.currentVillage.resources.wood > $rootScope.globals.currentVillage.resources.storage_space) {
								$rootScope.globals.currentVillage.resources.wood = $rootScope.globals.currentVillage.resources.storage_space;
							}

							if($rootScope.globals.currentVillage.resources.clay > $rootScope.globals.currentVillage.resources.storage_space) {
								$rootScope.globals.currentVillage.resources.clay = $rootScope.globals.currentVillage.resources.storage_space;
							}

							GameService.get_village_timers($rootScope.globals.currentVillage.id).then(function(data){ });
						}
					}
				}
			}

			// calculate army trainings
			if($rootScope.globals.currentVillage && $rootScope.globals.currentVillage.army) {
				if($rootScope.globals.currentVillage.army.units_training){
					for(var key in $rootScope.globals.currentVillage.army.units_training) {
						var ut = $rootScope.globals.currentVillage.army.units_training[key];
						ut.time_left = ut.start_at + ut.training_time - server_time;
					}
				}
			}
		}

		$interval(function() {
			$rootScope.update_timers();
		}, 1000);

		// keep user logged in after page refresh
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Token ' + $rootScope.globals.currentUser.loginToken; // jshint ignore:line
	        GameService.get_villages().then(function(data) {});
	        GameService.get_player_info($rootScope.globals.currentUser.user_id).then(function(resp) {
	        	$rootScope.globals.currentUser.clan = resp.clan;
	        	$rootScope.$broadcast('user_reload', {});
	        });
	        GameService.get_unread_badges().then(function(data) {});
        }

		$rootScope.go = function ( path ) {
		  	$location.path( path );
		};

		$rootScope.$on("$locationChangeStart", function (event, next, current, AuthenticationService) {
			ngProgress.start();
			if($location.path() === '/logout'){
	            $rootScope.globals = {};
	            $cookieStore.remove('globals');
	            $http.defaults.headers.common.Authorization = 'Basic ';
			}

			if(($location.path() === '/register' || $location.path() === '/login') && $rootScope.globals.currentUser ||
				$location.path() === '/create-village' && $rootScope.globals.villages.length > 0){
				redirect.path('/');
			}

		    // redirect to login page if not logged in
            if ($location.path() !== '/login' && $location.path() !== '/register' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }

            if($rootScope.globals && $rootScope.server_data && !$rootScope.server_data.time){
            	GameService.server_time().then(function(data) {});
        	}
		});

		$rootScope.$on("$routeChangeSuccess", function(event, currentRoute, previousRoute) {
			ngProgress.complete();
		});
	})
})();