(function(){
	var app = angular.module('gameApp');

	app.controller('menuController', ['$location', '$rootScope', function($location, $rootScope){
		this.isUserLoggedIn= function(){
			return $rootScope.globals.currentUser
		}
		this.isActive = function(path) {
			if($location.path() == path)
				return true;
			if($location.path().substring(0, path.length) == path && path.length > 1)
				return true;
			return false;
		};
	}]);

	app.controller('authController', ['AuthenticationService', '$location', '$rootScope',
			function(AuthenticationService, $location, $rootScope) {
		if($rootScope.globals.currentUser){
			$location.path('/');
		}

		this.login = function(credentials) {
			AuthenticationService.login(credentials);
		};
	}]);


	app.controller('registerController', ['$rootScope', '$location', 'AuthenticationService',
			function($rootScope, $location, AuthenticationService) {
		if($rootScope.globals.currentUser){
			$location.path('/');
		}

		this.credentials = {
			username_used: false,
			email_used: false
		};
		this.register = function(){
			AuthenticationService.register(this.credentials);
		}
	}]);

	app.controller('globalController', ['$scope', '$rootScope', 'CalcService', function($scope, $rootScope, CalcService){
        $scope.global_get_icon = function(name){
            if(name == 'castle') return 'castle-icon';
            if(name == 'farm') return 'farm-icon';
            if(name == 'stone_mine') return 'stone-mine-icon';
            if(name == 'lumber_camp') return 'lumber-camp-icon';
            if(name == 'clay_pit') return 'clay-pit-icon';
            if(name == 'storage') return 'storage-icon';
            if(name == 'grass') return 'grass-icon';
            if(name == 'forest') return 'forest-icon';
            if(name == 'mountain') return 'mountains-icon';
            if(name == 'sea') return 'at-sea-icon';
            if(name == 'barracks') return 'barracks-icon';
            if(name == 'wall') return 'wall-icon';
            if(name == 'market') return 'market-icon';
            if(name == 'stone') return 'stone-pile';
            if(name == 'wood') return 'wood-pile';
            if(name == 'clay') return 'spiral-bloom';
            if(name == 'embassy') return 'embassy-icon';
            return '';
        }

        $scope.global_get_icon_path = function(name){
            var prefix = '/static/frontend/images/'
            if(name == 'axe-icon') return prefix + 'wood-axe.svg';
            if(name == 'shield-icon') return prefix + 'cracked-shield.svg';
            if(name == 'message-icon') return prefix + 'tied-scroll.svg';
            if(name == 'attack') return prefix + 'wave-strike.svg';
            if(name == 'grass') return prefix + 'grass.svg';
            if(name == 'forest') return prefix + 'forest.svg';
            if(name == 'mountain') return prefix + 'mountains.svg';
            if(name == 'sea') return prefix + 'at-sea.svg';
            return '';
        }

		$scope.global_calc_prop = function(building_type, prop){
			return CalcService.calc_prop(building_type, prop);
		}
		$scope.global_calc_cost = function(building, prop){
			return CalcService.calc_cost(building, prop);
		}
	}]);

	app.controller('createVillageController', ['$rootScope', '$scope', '$location', 'GameService',
			function($rootScope, $scope, $location, GameService) {
		console.log($rootScope.globals.villages);
		if($rootScope.globals.villages.length > 0){
			$location.path('/village');
		}
		this.data = {
			location: null
		};
		$scope.villageCounter = {
			nw: 0, ne: 0, se: 0, sw: 0,
		};

		GameService.creating_map_info().then(function(data){
			$scope.villageCounter = data;
		});

		this.isSelected = function(class_name){
			return this.data.location == class_name;
		};
		this.select = function(class_name){
			this.data.location = class_name;
		}
		this.create = function(){
			GameService.create_village(this.data).then(function(data) {
				if(data.status == 200){
					$location.path('/village');
				}
			});
		}
	}]);

	app.controller('buildingsController', ['$rootScope', '$scope', '$routeParams', '$modal', 'messageCenterService', 'GameService', 'CalcService',
            function($rootScope, $scope, $routeParams, $modal, messageCenterService, GameService, CalcService) {
		$scope.building = {
			'resources': { 'clay': 0, 'wood': 0, 'stone': 0, 'building_time': 0, 'population': 0}
		};
        $scope.buildingsToBuild = false;
		$scope.building_template = '';

		if($routeParams.building_type == 'castle'){
			$scope.building_template = '/views/buildings/castle.html';
		} else if($routeParams.building_type == 'lumber_camp'){
			$scope.building_template = '/views/buildings/lumber-camp.html';
		} else if($routeParams.building_type == 'clay_pit'){
			$scope.building_template = '/views/buildings/clay-pit.html';
		} else if($routeParams.building_type == 'stone_mine'){
			$scope.building_template = '/views/buildings/stone-mine.html';
		} else if($routeParams.building_type == 'farm'){
			$scope.building_template = '/views/buildings/farm.html';
		} else if($routeParams.building_type == 'storage'){
			$scope.building_template = '/views/buildings/storage.html';
		} else if($routeParams.building_type == 'barracks'){
			$scope.building_template = '/views/buildings/barracks.html';
		} else if($routeParams.building_type == 'wall'){
			$scope.building_template = '/views/buildings/wall.html';
		} else if($routeParams.building_type == 'market'){
            $scope.building_template = '/views/buildings/market.html';
        } else if($routeParams.building_type == 'embassy'){
            $scope.building_template = '/views/buildings/embassy.html';
        }

		$scope.canBuild = function(name){
			if(!$rootScope.globals.currentVillage.buildings){
				return false;
			}

			for(var i = 0; i < $rootScope.globals.currentVillage.buildings.length; i++){
				if($rootScope.globals.currentVillage.buildings[i].building_type == name){
                    return false;
				}
			}

            var castle = null
            for(var i = 0; i < $rootScope.globals.currentVillage.buildings.length; i++){
                if($rootScope.globals.currentVillage.buildings[i].building_type == 'castle'){
                    castle = $rootScope.globals.currentVillage.buildings[i]
                }
            }

            if(!castle) { return false; }

            if($rootScope.globals.buildings[name].min_castle_stage <= castle.stage)
                return true;
    
			return false;
		}

		$scope.upgrade = function(stage){
			GameService.upgrade_building(
				$scope.building.building_type,
				$rootScope.globals.currentVillage.id,
				stage).then(function(data){
					if(data.success){
						var res = $rootScope.globals.currentVillage.resources;
						res.clay -= $scope.building.resources.clay;
						res.stone -= $scope.building.resources.stone;
						res.wood -= $scope.building.resources.wood;
						res.population += $scope.building.resources.population;
                        $scope.building.myBuilding.upgrading = true;
					}
			});
		}

		$scope.startTraining = function() {
			var obj = {
				'units': [],
				'village_id': $rootScope.globals.currentVillage.id
			}

			var used_resources = {
				stone: 0,
				wood: 0,
				clay: 0,
				population: 0
			}

			for (var unit_type in $scope.building.data.units) {
				if ($scope.building.data.units.hasOwnProperty(unit_type)) {
			    	var unit = $scope.building.data.units[unit_type];
			    	if (unit.start_training === parseInt(unit.start_training, 10)){
			    		used_resources.stone += unit.training_cost.stone * unit.start_training;
			    		used_resources.wood += unit.training_cost.wood * unit.start_training;
			    		used_resources.clay += unit.training_cost.clay * unit.start_training;
			    		used_resources.population += unit.training_cost.population * unit.start_training;
				    	obj.units.push({
				    		'unit_type': unit_type,
				    		'number': unit.start_training
				    	})
			    	}
			    }
			}
			if(obj.units.length > 0){
				GameService.start_training_units(obj).then(function(data){
					if(data.units_training) {
						$rootScope.globals.currentVillage.army.units_training = data.units_training;
			    		$rootScope.globals.currentVillage.resources.stone -= used_resources.stone;
			    		$rootScope.globals.currentVillage.resources.wood -= used_resources.wood;
			    		$rootScope.globals.currentVillage.resources.clay -= used_resources.clay;
			    		$rootScope.globals.currentVillage.resources.population += used_resources.population;
					}
				});
			}
		}

        $scope.create_clan = function() {
            var modalInstance = $modal.open({
              templateUrl: '/views/directives/create-clan-modal.html',
              controller: 'createClanModalInstanceCtrl',
              resolve: {
                clan: function() {
                    return null;
                }
              }
            })
            modalInstance.result.then(function(clan) {
                $rootScope.globals.currentUser.clan = {
                    id: clan.id,
                    name: clan.name
                }
            },function(){
                console.log("Modal Closed");
            });
        }

        $scope.leave_clan = function() {
            if (confirm("Are you sure you want to leave clan?") == true) {
                GameService.leave_clan($rootScope.globals.currentUser.clan.id, $rootScope.globals.currentUser.user_id).then(function() {
                    messageCenterService.add('success', 'You leaved clan!', {});
                    $rootScope.globals.currentUser.clan = null;
                });
            }
        }

        $scope.accept_invite = function(invite) {
            GameService.accept_clan_invite(invite.clan.id).then(function(resp) {
                if(resp.success == true) {
                    $rootScope.globals.currentUser.clan = invite.clan;
                    $scope.clan_invites.splice($scope.clan_invites.indexOf(invite), 1);      
                }
            });
        }

        function buildingsToBuild() {
            var buildings = ['barracks', 'wall', 'market', 'embassy'];

            for(var x = 0; x < buildings.length; x++) {
                found = false;
                for(var i = 0; i < $rootScope.globals.currentVillage.buildings.length; i++){
                    if($rootScope.globals.currentVillage.buildings[i].building_type == buildings[x]){
                        found = true;
                    }
                }
                if(!found) {
                    $scope.buildingsToBuild = true;
                    return;
                }
            }
            $scope.buildingsToBuild = false;
        }

		// Init building data
		function init_building(){
			$scope.building.building_type = $routeParams.building_type,
			$scope.building.data = $rootScope.globals.buildings[$routeParams.building_type];
			$scope.$broadcast('building_loaded', {});
		}

		// Load current village
		function load_current_village(){
			var found = false;
			for(var i = 0; i < $rootScope.globals.currentVillage.buildings.length; i++){
				if($scope.building.building_type == $rootScope.globals.currentVillage.buildings[i].building_type){
					$scope.building.myBuilding = $rootScope.globals.currentVillage.buildings[i];
					$scope.building.resources.stone = CalcService.calc_cost($scope.building, 'stone');
					$scope.building.resources.wood = CalcService.calc_cost($scope.building, 'wood');
					$scope.building.resources.clay = CalcService.calc_cost($scope.building, 'clay');
					$scope.building.resources.building_time = CalcService.calc_cost($scope.building, 'building_time');
					$scope.building.resources.population = CalcService.calc_cost($scope.building, 'population');

					if($scope.building.building_type == 'castle'){
						$scope.building.building_speed = CalcService.calc_cost($scope.building, 'building_speed');
					}else if($scope.building.building_type == 'barracks'){
						$scope.building.training_speed = CalcService.calc_cost($scope.building, 'training_speed');
					} else if($scope.building.building_type == 'wall'){
						$scope.building.defense_rate = CalcService.calc_cost($scope.building, 'defense_rate');
					}
					found = true;
					break;
				}
			}
			if(!found){
				$scope.building.myBuilding = {building_type: $scope.building.building_type, stage: 0}
				$scope.building.resources.stone = CalcService.calc_cost($scope.building, 'stone');
				$scope.building.resources.wood = CalcService.calc_cost($scope.building, 'wood');
				$scope.building.resources.clay = CalcService.calc_cost($scope.building, 'clay');
				$scope.building.resources.building_time = CalcService.calc_cost($scope.building, 'building_time');
				$scope.building.resources.population = CalcService.calc_cost($scope.building, 'population');

				if($scope.building.building_type == 'castle'){
					$scope.building.building_speed = CalcService.calc_cost($scope.building, 'building_speed');
				}else if($scope.building.building_type == 'barracks'){
					$scope.building.training_speed = CalcService.calc_cost($scope.building, 'training_speed');
				}
			}
			$scope.$broadcast('building_loaded', {});
            buildingsToBuild();
            if($scope.building.building_type == 'embassy') {
                GameService.get_player_clan_invitations().then(function(resp) {
                    $scope.clan_invites = resp;
                });
            }
		}

		// Load Buildings basic data
		if(!$rootScope.globals.buildings){
			GameService.get_buildings_data().then(function(data){
				init_building();

				// Load current village
				if(!$rootScope.globals.currentVillage.buildings){
					GameService.get_full_village_data().then(function(data) {
						load_current_village();
					});
				}else{
					load_current_village();
				}
			});
		}else{
			init_building();
			load_current_village();
		}
	}]);

    app.controller('createClanModalInstanceCtrl',['$modalInstance', '$scope', 'GameService', 'messageCenterService', 'clan',
            function($modalInstance, $scope, GameService, messageCenterService, clan) {
        $scope.data = {};
        $scope.edit = false;
        if(clan) {
            $scope.edit = true;
            $scope.data = clan;
        }

        $scope.create_clan = function() {
            if($scope.edit) {
                GameService.edit_clan($scope.data.id, $scope.data).then(function(resp) {
                    $modalInstance.close(resp);
                });
            } else {
                GameService.create_clan($scope.data).then(function(resp) {
                    $modalInstance.close(resp);
                });
            }
        }
    }]);

	app.controller('villageController', ['$rootScope', '$scope', 'GameService', function($rootScope, $scope, GameService) {
		$scope.village = {};
		$scope.buildings = {};

		$scope.hasBuilding = function(name){
			if(!$scope.village.buildings){
				return false;
			}
			for(var i = 0; i < $scope.village.buildings.length; i++){
				if($scope.village.buildings[i].building_type == name){
					return true;
				}
			}
			return false;
		}
		$scope.$on('reloadVillage', function(event, data) { load_village_data(); });

		// Load village data
		function load_village_data(){
			$scope.village = $rootScope.globals.currentVillage;
			angular.forEach($scope.village.buildings, function(value, key){
				$scope.buildings[value.building_type] = value;
			});
		}

		if(!$rootScope.globals.villages || $rootScope.globals.villages.length == 0){
	        GameService.get_villages().then(function(data){
	        	if($rootScope.globals.villages.length > 0){
					GameService.get_full_village_data().then(function(data) {});
				}
	        });
		}else{
			if(!$rootScope.globals.currentVillage.buildings){
				GameService.get_full_village_data().then(function(data) {});
			}else{
				load_village_data();
			}
		}
	}]);

	app.controller('mapController', ['$rootScope', '$scope', 'GameService', function($rootScope, $scope, GameService) {
		$scope.map = [];
		$scope.line_length = 0;
		$scope.center = {}

		$scope.move_map = function(direction){
			if(direction == 'up'){
				$scope.center.y += 5;
			} else if(direction == 'down'){
				$scope.center.y -= 5;
			} else if(direction == 'left'){
				$scope.center.x -= 5;
			} else if(direction == 'right'){
				$scope.center.x += 5;
			}
			init_map();
		}

		function init_map(){
			if(!$scope.center.x){
				$scope.center = $rootScope.globals.currentVillage.coordinates;
			}

			GameService.get_map($scope.center.x, $scope.center.y).then(function(data){
				$scope.map = data['map'];
				$scope.line_length = Math.sqrt($scope.map.length);
			});
		}

		if(!$rootScope.globals.currentVillage.id) {
			GameService.get_full_village_data().then(function(data) {
				init_map();
			});
		} else {
			init_map();
		}
		
	}]);

	app.controller('coordinateController', ['$scope', '$rootScope', '$routeParams', 'GameService',
			function($scope, $rootScope, $routeParams, GameService) {
		$scope.info = {};

		$scope.sendUnits = function() {
			var obj = {
				'units': [],
				'village_id': $rootScope.globals.currentVillage.id,
				'target_village_id': $scope.info.village.village_id,
				'type': 'A'
			}
			for (var unit_type in $rootScope.globals.currentVillage.army.units) {
				if ($rootScope.globals.currentVillage.army.units.hasOwnProperty(unit_type)) {
			    	var unit = $rootScope.globals.currentVillage.army.units[unit_type];
			    	if (unit.send_unit === parseInt(unit.send_unit, 10)){
				    	obj.units.push({
				    		'unit_type': unit.unit_type,
				    		'number': unit.send_unit
				    	})
			    	}
			    }
			}

			if(obj.units.length > 0){
				GameService.send_units(obj).then(function(data){
					if(data.army_movements){
						$rootScope.globals.currentVillage.timers.army_movement = data.army_movements;

						// reset units number
						for (var unit_type in $rootScope.globals.currentVillage.army.units) {
							if ($rootScope.globals.currentVillage.army.units.hasOwnProperty(unit_type)) {
						    	var unit = $rootScope.globals.currentVillage.army.units[unit_type];
                                if(unit.send_unit != null) {
						    	    unit.number -= unit.send_unit;
						    	    unit.send_unit = null;
                                }
						    }
						}
					}
				});
			}
		}

		GameService.get_coorinate_info($routeParams.x, $routeParams.y).then(function(data){
			$scope.info = data;
		});

		GameService.get_full_village_data().then(function(data) { });
	}]);

	app.controller('sendMessageModalInstanceCtrl',['$modalInstance', '$scope', 'player', 'GameService', 'messageCenterService',
			function($modalInstance, $scope, player, GameService, messageCenterService) {
		$scope.data = {};
		$scope.player = player;

		$scope.send_message = function() {
			$scope.data.receiver_id = $scope.player.id;
			GameService.send_message($scope.data).then(function(resp) {
				if(resp.success == true) {
					messageCenterService.add('success', 'Message was sent!', {});	
				}
				$modalInstance.close();
			});
		}
	}]);

	app.controller('reportsController', ['$scope', '$rootScope', 'GameService', function($scope, $rootScope, GameService) {
		$scope.reports = [];
		$scope.totalItems = 0;
		$scope.currentPage = 1;
		$scope.totalPages = 1;
		$scope.maxSize = 5;

		 $scope.pageChanged = function () {
		 	load_reports();
		};

		load_reports();

		function load_reports() {
			GameService.get_reports($scope.currentPage).then(function(data) {
				$scope.reports = data.reports;
				$scope.totalItems = data.num_of_reports;
			});
		}
	}]);

	app.controller('reportController', ['$scope', '$rootScope', '$routeParams', 'GameService', function($scope, $rootScope, $routeParams, GameService) {
		$scope.report = {}

        console.log($rootScope.globals)

		GameService.get_report($routeParams.id).then(function(data){
			$scope.report = data;
            GameService.get_unread_badges().then(function(data) {});
		})
	}]);

	app.controller('marketController', ['$scope', '$rootScope', 'GameService', 'CalcService', 'messageCenterService',
			function($scope, $rootScope, GameService, CalcService, messageCenterService) {
		$scope.totalTraders = 0;
		$scope.usedTraders = 0;
		$scope.tradersUsedInCalculation = 0;
		$scope.maxTradeCapacity = 0;
		$scope.max_value = 0;
		$scope.carrying_capacity = 0;
		$scope.myOffers = [];
		$scope.publicOffers = [];
 
		$scope.data = {
			offered: {
				resource: 'stone',
				value: 0
			},
			requested: {
				resource: 'stone',
				value: 0
			}
		}

		$scope.calculate_total_resources = function() {
			var avaliableTraders = $scope.totalTraders - $scope.usedTraders;
			$scope.maxTradeCapacity = avaliableTraders * $scope.carrying_capacity;
			var resources = $rootScope.globals.currentVillage.resources;

			$scope.max_value = $scope.maxTradeCapacity;

			if(resources == undefined) { return }

			if($scope.data.offered.value && $scope.data.offered.value > $scope.max_value) {
				$scope.data.offered.value = $scope.max_value;
			}
			if($scope.data.requested.value && $scope.data.requested.value > $scope.max_value) {
				$scope.data.requested.value = $scope.max_value;
			}

			if($scope.data.offered.resource == 'stone') {
				if($scope.maxTradeCapacity > resources.stone) {
					$scope.max_value = resources.stone;
				}
			} else if($scope.data.offered.resource == 'wood') {
				if($scope.maxTradeCapacity > resources.wood) {
					$scope.max_value = resources.wood;
				}
			} else if($scope.data.offered.resource == 'clay') {
				if($scope.maxTradeCapacity > resources.clay) {
					$scope.max_value = resources.clay;
				}
			}
			var usedTradeCapacity = 0;
			if($scope.data.offered.value) {
				usedTradeCapacity = $scope.data.offered.value;
			}

			if($scope.data.requested.value && $scope.data.requested.value > usedTradeCapacity) {
				usedTradeCapacity = $scope.data.requested.value;
			}

			$scope.tradersUsedInCalculation = Math.ceil((usedTradeCapacity) / $scope.carrying_capacity)
		}

		$scope.add_resources_to_input = function(type) {
			if(type == 'offering') {
				$scope.data.offered.value = $scope.max_value;
			} else {
				$scope.data.requested.value = $scope.max_value;
			}
			$scope.calculate_total_resources();	
		}

		$scope.create_offer = function(data) {
			if(!$scope.data.offered.value || $scope.data.offered.value == 0) return;
			if(!$scope.data.requested.value || $scope.data.requested.value == 0) return;

			$scope.data.village_id = $rootScope.globals.currentVillage.id;
			GameService.create_market_offer($scope.data).then(function(resp) {
				if(resp.offered.resource == 'stone') {
					$rootScope.globals.currentVillage.resources.stone -= resp.offered.value;
				}else if(resp.offered.resource == 'wood') {
					$rootScope.globals.currentVillage.resources.wood -= resp.offered.value;
				}else if(resp.offered.resource == 'clay') {
					$rootScope.globals.currentVillage.resources.clay -= resp.offered.value;
				}

				$scope.myOffers.push(resp);
			});
		}

		$scope.accept_offer = function(offer) {
			if(offer.requested.resource == 'stone' && $rootScope.globals.currentVillage.resources.stone < offer.requested.value ||
			   offer.requested.resource == 'wood' && $rootScope.globals.currentVillage.resources.wood < offer.requested.value ||
			   offer.requested.resource == 'clay' && $rootScope.globals.currentVillage.resources.clay < offer.requested.value) {
	            	messageCenterService.add('danger', 'Not enough resources!', {});
					return;
			}

			GameService.accept_market_offer(offer.id).then(function(resp) {
				if(resp.error == 2002) { return; }

				index = null;
				for(var i = 0; i < $scope.publicOffers.length; i++) {
					if(offer.id == $scope.publicOffers[i].id) {
						index = i;
						break;
					}
				}

				if(index) {
					$scope.publicOffers.splice(index, 1);
				}

				if(resp.error) { return; }
				if(resp.requested.resource == 'stone') {
					$rootScope.globals.currentVillage.resources.stone -= resp.requested.value;
				} else if(resp.requested.resource == 'wood') {
					$rootScope.globals.currentVillage.resources.wood -= resp.requested.value;
				} else if(resp.requested.resource == 'clay') {
					$rootScope.globals.currentVillage.resources.clay -= resp.requested.value;
				}

				$rootScope.globals.currentVillage.timers.market_movement.push(resp);
			});
		}

		function init_data() {
			if(!$scope.$parent.building.data) { return; }
			$scope.totalTraders = CalcService.calc_cost($scope.$parent.building, 'traders');
			$scope.carrying_capacity = $scope.$parent.building.data.carrying_capacity;
			$scope.calculate_total_resources();

			GameService.get_market_offers('my').then(function(data) {
				$scope.myOffers = data.offers;
				$scope.usedTraders = data.used_traders;
				$scope.calculate_total_resources();
			});
			GameService.get_market_offers().then(function(data) {
				$scope.publicOffers = data.offers;
			});
		}

		$scope.$on('building_loaded', function(data) {
			init_data();
		});

		$scope.$on('reloadVillage', function(data) {
			init_data();
		});
		init_data();
	}]);

	app.controller('messagesController', ['$scope', '$location', 'GameService', function($scope, $location, GameService) {
		$scope.messages = [];
		$scope.totalItems = 0;
		$scope.currentPage = 1;
		$scope.totalPages = 1;
		$scope.maxSize = 5;
        $scope.tabs = [
            { title: 'Inbox', active: $location.path() == '/messages', url: '/messages' },
            { title: 'Sent', active: $location.path() == '/messages/sent', url: '/messages/sent' }
        ]

        $scope.tabChange = function(path) {
            if(path) {
                $location.path(path);
            }
            
            load_messages();
        }

		$scope.pageChanged = function () {           
		 	load_messages();
		};

		load_messages();

		function load_messages() {
			var m_type = 'received'
			if($location.path() == '/messages/sent') {
				m_type = 'sent';
			}

			GameService.get_messages(m_type, $scope.currentPage).then(function(data) {
				$scope.messages = data.messages;
				$scope.totalItems = data.num_of_messages;
			});
		}
	}]);

    app.controller('messageController', ['$scope', '$rootScope', '$routeParams', 'GameService', function($scope, $rootScope, $routeParams, GameService) {
        $scope.message = {}

        GameService.get_message($routeParams.id).then(function(data){
            $scope.message = data;
            GameService.get_unread_badges().then(function(data) {});
        })
    }]);

    app.controller('playerController', ['$scope', '$rootScope', '$routeParams', '$modal', 'GameService',
            function($scope, $rootScope, $routeParams, $modal, GameService) {
        $scope.player = {}
        $scope.invition_already_sent = true;

        $scope.write_message = function() {
            var modalInstance = $modal.open({
              templateUrl: '/views/directives/send-message-modal.html',
              controller: 'sendMessageModalInstanceCtrl',
              resolve: {
                player: function() {
                    return $scope.player;
                }
              }
            })
            modalInstance.result.then(function(edition){
            },function(){
                console.log("Modal Closed");
            });
        }

        $scope.invite_to_clan = function() {
            GameService.invite_to_clan($scope.player.id).then(function(resp) {
                console.log(resp);
                if(resp.success == true) {
                    $scope.invition_already_sent = true;
                }
            });
        }

        GameService.get_player_info($routeParams.id).then(function(data){
            $scope.player = data;
            if($rootScope.globals.currentUser.clan &&
                $rootScope.globals.currentUser.user_id == $rootScope.globals.currentUser.clan.leader.id &&
                (!$scope.player.clan || $rootScope.globals.currentUser.clan.id != $scope.player.clan.id)) {
                GameService.has_clan_invitation($rootScope.globals.currentUser.clan.id, $routeParams.id).then(function(resp) {
                    if(resp.exist == false) {
                        $scope.invition_already_sent = false;
                    }
                });
            }
        });

    }]);

    app.controller('clanController', ['$scope', '$routeParams', '$rootScope', '$modal', 'messageCenterService', 'GameService',
            function($scope, $routeParams, $rootScope, $modal, messageCenterService, GameService) {
        $scope.clan = {}
        $scope.clan_leader = false;
        $scope.invites = [];

        $scope.edit_clan = function() {
            var modalInstance = $modal.open({
              templateUrl: '/views/directives/create-clan-modal.html',
              controller: 'createClanModalInstanceCtrl',
              resolve: {
                clan: function() {
                    return $scope.clan;
                }
              }
            })
            modalInstance.result.then(function(clan) {
                $scope.clan.name = clan.name;
                $scope.clan.description = clan.description;
            },function(){
                console.log("Modal Closed");
            });
        }

        $scope.remove_player = function(member) {
            GameService.leave_clan($scope.clan.id, member.id).then(function() {
                messageCenterService.add('success', member.username + ' was removed from clan!', {});
                $scope.clan.members.splice($scope.clan.members.indexOf(member), 1);
            });
        }

        $scope.remove_invite = function(invite) {
            GameService.remove_invite($scope.clan.id, invite.id).then(function() {
                messageCenterService.add('success', 'Invite was removed!', {});
                $scope.invites.splice($scope.invites.indexOf(invite), 1);
            });
        }

        GameService.get_clan_info($routeParams.id).then(function(resp) {
            $scope.clan = resp;
            $scope.clan_leader = $rootScope.globals.currentUser.user_id == $scope.clan.leader.id;
            if($scope.clan_leader) {
                GameService.get_clan_invitations($scope.clan.id).then(function(resp) {
                    $scope.invites = resp;
                });
            }
        });
    }]);

    app.controller('forumController', ['$scope', '$rootScope', '$modal', 'GameService', function($scope, $rootScope, $modal, GameService) {
        $scope.topics = [];
        $scope.editor = false;

        $scope.add_topic = function() {
            var modalInstance = $modal.open({
              templateUrl: '/views/directives/topic-modal.html',
              controller: 'topicModalInstanceCtrl',
              resolve: {}
            })
            modalInstance.result.then(function(topic) {
                $scope.topics.push(topic)
            },function(){
                console.log("Modal Closed");
            });
        }

        GameService.get_topics().then(function(resp) {
            $scope.topics = resp;
        });

        function set_editor_mode() {
            if($rootScope.globals.currentUser && $rootScope.globals.currentUser.clan && $rootScope.globals.currentUser.clan.leader) {
                $scope.editor = $rootScope.globals.currentUser.user_id == $rootScope.globals.currentUser.clan.leader.id;
            }
        }

        set_editor_mode();

        $rootScope.$on('user_reload', function() { set_editor_mode(); });
    }]);

    app.controller('topicModalInstanceCtrl',['$modalInstance', '$scope', 'GameService', 'messageCenterService',
            function($modalInstance, $scope, GameService, messageCenterService) {
        $scope.data = {};

        $scope.add_topic = function() {
            GameService.add_topic($scope.data).then(function(resp) {
                messageCenterService.add('success', 'Topic was added!', {});
                $modalInstance.close(resp);
            });
        }
    }]);

    app.controller('topicController', ['$scope', '$routeParams', 'GameService', function($scope, $routeParams, GameService) {
        $scope.topic = null;
        $scope.data = {}
        $scope.posts = [];
        $scope.last_seen = [];
        $scope.currentPage = 2;
        $scope.totalItems = 0;
        $scope.maxSize = 5;

        $scope.write_post = function() {
            if($scope.data.content) {
                GameService.write_post($scope.topic.id, $scope.data).then(function(resp) {
                    $scope.posts.unshift(resp);
                    $scope.data = {};
                });
            }
        }

        $scope.pageChanged = function () {
            console.log($scope);       
            load_posts();
        };

        function load_posts() {
            GameService.get_posts($routeParams.id, $scope.currentPage).then(function(data) {
                $scope.posts = data.posts;
                $scope.totalItems = data.num_of_posts;
            });
        }

        GameService.get_topic($routeParams.id).then(function(data) {
            $scope.topic = data;
        load_posts();
            console.log($scope.topic);
        });
    }]);

})();
