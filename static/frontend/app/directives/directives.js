(function(){
	var app = angular.module('gameApp');

	app.directive('menuBlock', function(){
		return {
			restrict: 'E',
			templateUrl: '/views/directives/menu.html',
		};
	});

	app.directive('resourcesBlock', function() {
		return {
			restrict: 'E',
			templateUrl: '/views/directives/resources.html',
		};
	});

	app.directive('timersBlock', function() {
		return {
			restrict: 'E',
			templateUrl: '/views/directives/timers.html',
		};
	});

	app.directive('numeric', function () {
	    return {
	      require: 'ngModel',
	      restrict: 'A',
	      link: function (scope, element, attr, ctrl) {
	        function inputValue(val) {
	          if (val) {
	            var digits = val.replace(/[^0-9]/g, '');

	            if (digits !== val) {
	              ctrl.$setViewValue(digits);
	              ctrl.$render();
	            }
	            return parseInt(digits);
	          }
	          return undefined;
	        }            
	        ctrl.$parsers.push(inputValue);
	      }
	    };
	});

	app.directive('myMaxlength', function() {
	  return {
	    require: 'ngModel',
	    link: function (scope, element, attrs, ngModelCtrl) {
	      var maxlength = Number(attrs.myMaxlength);
	      function fromUser(text) {
	          if (text.length > maxlength) {
	            var transformedInput = text.substring(0, maxlength);
	            ngModelCtrl.$setViewValue(transformedInput);
	            ngModelCtrl.$render();
	            return transformedInput;
	          } 
	          return text;
	      }
	      ngModelCtrl.$parsers.push(fromUser);
	    }
	  }; 
	});

	app.filter('orderObjectBy', function() {
		return function(items, field, reverse) {
		    var filtered = [];
		    angular.forEach(items, function(item, key) {
		    	item.key = key;
		      filtered.push(item);
		    });
		    filtered.sort(function (a, b) {
		      return (a[field] > b[field] ? 1 : -1);
		    });
		    if(reverse) filtered.reverse();
		    return filtered;
		};
	});

	app.filter('duration', function() {
	    return function(seconds) {
	        var h = 3600;
	        var m = 60;
	        var hours = Math.floor(seconds/h);
	        var minutes = Math.floor( (seconds % h)/m );
	        var scnds = Math.floor( (seconds % m) );
	        var timeString = '';
	        if(scnds < 10 && ( hours > 0 || minutes > 0 )) scnds = "0"+scnds;
	        if(minutes < 10 && hours > 0) minutes = "0"+minutes;
	        if(hours > 0) timeString = hours +":"+ minutes +":"+scnds;
	        else if(minutes > 0) timeString = minutes +":"+scnds;
	        else timeString = scnds;
	        //timeString = hours +":"+ minutes +":"+scnds;
	        return timeString;
	    }
	});
})();