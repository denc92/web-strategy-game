(function() {
	var app = angular.module('gameApp')
 
	app.factory('AuthenticationService',
	    ['$http', '$cookieStore', '$rootScope', '$timeout', '$location', 'ngProgress', 'messageCenterService', 'GameService',
	    function ($http, $cookieStore, $rootScope, $timeout, $location, ngProgress, messageCenterService, GameService) {
	        var service = {};

	        service.login = function (credentials, callback) {
				ngProgress.start();
				$http.post($rootScope.server + '/api/login/', {
					"username": credentials.username,
					"password": credentials.password
				}).success(function(response) {
					$rootScope.globals = {};
					$rootScope.globals.currentUser = {
	                    username: credentials.username,
	                    loginToken: response.token,
	                    user_id: response.user_id
	                };

		         	GameService.init_user_data();
		 
		            $http.defaults.headers.common['Authorization'] = 'Token ' + response.token;
		            $cookieStore.put('globals', $rootScope.globals);
            		GameService.get_unread_badges().then(function(data) {});

					$location.path('/village');
	            }).error(function(data, status) {
	            	if(status == 403){
	            		messageCenterService.add('danger', 'Invalid username or password!', {});
	            	}else{
	            		messageCenterService.add('danger', 'Something went wrong!', {});
	            	}
	            }).finally(function(){
	            	ngProgress.complete();
	            });
	        };
	 
	        service.register = function (credentials, loginToken) {
	            ngProgress.start();
				$http.post($rootScope.server + '/api/register/', {
					"username": credentials.username,
					"email": credentials.email,
					"password": credentials.password
				}).success(function(response) {
					if(response.success){
						messageCenterService.add('success', 'Successfully registered!', {});
						$location.path('/login');
					}else{
						for(var i = 0; i < response.errors.length; i++){
							if(response.errors[i] == 1000){
								credentials.username_used = true;
							}else if(response.errors[i] == 1001){
								credentials.email_used = true;
							}
						}
					}
	            }).error(function(data, status) {
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	ngProgress.complete();
	            });
	        };
	 
	        return service;
	}]);

	app.factory('CalcService', ['$rootScope', function($rootScope) {
		var service = {};

		function calc_res_cost(resource, stage, factor, res, bonus){
				if(resource == 'building_speed' || resource == 'training_speed' || resource == 'defense_rate'){
					for(var i = 1; i < stage; i++){
						res = res + factor;
					}
				}else{
					for(var i = 1; i < stage; i++){
						res += res * factor;
					}
				}

        		return Math.ceil(res + (res * bonus));
			}

		service.calc_cost = function(building, resource){
			if(!building || !building.data || !building.myBuilding){
				return '';
			}
			if(building.myBuilding.stage >= building.data.max_stage){
				return 0;
			}

			if(resource == 'population'){
				return building.data.building_cost.population;
			}
			var res_cost = 0;
			var factor = building.data.building_cost.cost_factor;
			var bonus = 0;
			var stage = building.myBuilding.stage;
			if(resource == 'building_time'){
				res_cost = building.data.building_cost.resources.time;
				for(var b in $rootScope.globals.currentVillage.buildings){
					var building = $rootScope.globals.currentVillage.buildings[b];
					if(building.building_type === 'castle'){
						bonus = building.stats.building_speed_increase;
						bonus = -1 * (bonus / 100)
					}
				}
				stage += 1;
			}else if(resource == 'stone'){
				res_cost = building.data.building_cost.resources.stone;
				stage += 1;
			}else if(resource == 'wood'){
				res_cost = building.data.building_cost.resources.wood;
				stage += 1;
			}else if(resource == 'clay'){
				res_cost = building.data.building_cost.resources.clay;
				stage += 1;
			}else if(resource == 'building_speed'){
				res_cost = building.data.building_cost.building_speed;
				factor = building.data.building_cost.building_factor;
			}else if(resource == 'training_speed'){
				res_cost = building.data.building_cost.training_speed;
				factor = building.data.building_cost.training_factor;
			}else if(resource == 'defense_rate'){
				res_cost = building.data.building_cost.defense_rate;
				factor = building.data.building_cost.defense_factor;
			}else if(resource == 'traders'){
				res_cost = building.data.building_cost.traders;
				factor = building.data.building_cost.traders_factor;
			}

			return calc_res_cost(resource, stage, factor, res_cost, bonus)
		}

		service.calc_prop = function(building_type, prop){
			var building = null;
			for(var i in $rootScope.globals.currentVillage.buildings){
				var b = $rootScope.globals.currentVillage.buildings[i];
				if(b.building_type == building_type){
					building = b;
					break;
				}
			}

			if(building){
				if(!$rootScope.globals.buildings || !$rootScope.globals.buildings[building_type]){
					return '';
				}
				var basic_data = $rootScope.globals.buildings[building_type];
				var res_cost = 0;
				var factor = 0;
				var bonus = 0;
				var stage = building.stage;
				if(prop == 'max_population'){
					res_cost = basic_data.building_cost.max_population;
					factor = basic_data.building_cost.population_factor;
				} else if(prop == 'income_speed'){
					res_cost = basic_data.building_cost.income_speed;
					factor = basic_data.building_cost.income_factor;
				} else if(prop == 'storage_space'){
					res_cost = basic_data.building_cost.storage_space;
					factor = basic_data.building_cost.storage_factor;
				}

				return calc_res_cost(prop, stage, factor, res_cost, bonus)
			}
			return '';
		}

		return service;
	}]);

	app.factory('GameService',
	    ['$http', '$cookieStore', '$rootScope', '$timeout', '$location', '$q', 'ngProgress', 'messageCenterService', 'CalcService',
	    function ($http, $cookieStore, $rootScope, $timeout, $location, $q, ngProgress, messageCenterService, CalcService) {
	        var service = {};
	        var _lock_get_villages = null;
	        var _lock_get_full_village_data = null;
	        var _lock_get_village_timers = null;
	        var _lock_get_buildings_data = null;
	        var _lock_upgrade_building = null;
	        var _lock_server_time = null;
	        var _lock_get_map = null;
	        var _lock_get_coordinate_info = null;

	        service.init_user_data = function() {
	        	$rootScope.globals.currentVillage = {};
				$rootScope.globals.villages = [];
				$rootScope.globals.timers = {
					resources: {
						synced: null
					},
					buildings: [],
					army: []
				}
	        }

	        service.get_villages = function (callback) {
	        	if(service._lock_get_villages){
	        		return service._lock_get_villages;
	        	}
				ngProgress.start();
				var deferred = $q.defer();

				$http.post($rootScope.server + '/api/villages/'
				).success(function(data) {
					$rootScope.globals.villages = data.villages;
					if(data.villages.length == 0){
						$location.path('/create-village');
					}
	            	deferred.resolve(data);

				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	service._lock_get_villages = null;
	            	ngProgress.complete();
	            })
	            service._lock_get_villages = deferred.promise;
	            return deferred.promise;
	        };

	        service.get_full_village_data = function(village_id, callback){
	        	var id = null;

	        	function try_to_get_village_id(){
	        		var id = null;
	        		if($rootScope.globals.currentVillage){
		        		id = $rootScope.globals.currentVillage.id;
		        	}
		        	if(!id && $rootScope.globals.villages){
		        		for(var i = 0; i < $rootScope.globals.villages.length; i++){
		        			if(id == null || id > $rootScope.globals.villages[i].id){
		        				id = $rootScope.globals.villages[i].id;
		        			}
		        		}
		        	}
		        	return id;
	        	}

	        	if(village_id){
	        		id = village_id;
	        	}else{
		        	id = try_to_get_village_id();
		        }

		        function load_village(id){
		        	if(service._lock_get_full_village_data){
		        		return service._lock_get_full_village_data;
		        	}

		        	if(!id){
		        		id = try_to_get_village_id()
		        	}

					ngProgress.start();
		        	var deferred = $q.defer();
					$http.post($rootScope.server + '/api/full-village-data/' + id + '/'
					).success(function(data) {
	            		$rootScope.globals.currentVillage = data.village;
	            		console.log(data.village);
	            		if($rootScope.globals.buildings){
	            			$rootScope.globals.currentVillage.resources.max_population = CalcService.calc_prop('farm', 'max_population');
	            			$rootScope.globals.currentVillage.resources.storage_space = CalcService.calc_prop('storage', 'storage_space');
	            		}else{
	            			service.get_buildings_data().then(function(data) {});
	            		}

	            		var timestamp = Math.ceil(new Date().getTime() / 1000);
	            		$rootScope.globals.timers.resources.synced = timestamp;
	            		$rootScope.globals.timers.resources.stone = timestamp;
	            		$rootScope.globals.timers.resources.wood = timestamp;
	            		$rootScope.globals.timers.resources.clay = timestamp;
	            		deferred.resolve(data);
	            		$rootScope.$broadcast('reloadVillage', data);
	            		service.get_village_timers(id).then(function(data){});
					}).error(function(data, status) {
						deferred.reject();
		            	messageCenterService.add('danger', 'Something went wrong!', {});
		            }).finally(function(){
		            	service._lock_get_full_village_data = null;
		            	ngProgress.complete();
		            })

		            service._lock_get_full_village_data = deferred.promise;
		            return deferred.promise;
		        }

		        if(!id){
		     		var deferred = $q.defer();

		        	service.get_villages().then(function(data){
		        		deferred.resolve(load_village(id));
		        	});
		        	return deferred.promise;
		        }else{
		        	return load_village(id);
		        }
	        }

	        service.server_time = function(callback){
	        	if(service._lock_server_time){
	        		return service._lock_server_time;
	        	}

	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/server-time/'
				).success(function(data) {
					$rootScope.server_data = {
						time: data.server_time,
						frontend_time: Math.ceil(new Date().getTime() / 1000)
					}
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	service._lock_server_time = null;
	            })

	            service._lock_server_time = deferred.promise;
	            return deferred.promise;
	        }

	        service.get_village_timers = function(village_id, callback){
	        	if(!village_id){
	        		return null;
	        	}

	        	if(service._lock_get_village_timers){
	        		return service._lock_get_village_timers;
	        	}

	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/village/' + village_id + '/timers/'
				).success(function(data) {
					if($rootScope.globals.currentVillage){
						$rootScope.globals.currentVillage.timers = data;
						$rootScope.globals.currentVillage.timers.frontend_time = Math.ceil(new Date().getTime() / 1000);
					}
					$rootScope.update_timers();
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	service._lock_get_village_timers = null;
	            })

	            service._lock_get_village_timers = deferred.promise;
	            return deferred.promise;
	        }

	        service.get_map = function(x, y, callback){
	        	if(service._lock_get_map){
	        		return service._lock_get_map;
	        	}

	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/map/' + x + '/' + y + '/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	service._lock_get_map = null;
	            })

	            service._lock_get_map = deferred.promise;
	            return deferred.promise;
	        }

	        service.get_coorinate_info = function(x, y, callback){
	        	if(service._lock_get_coordinate_info) {
	        		return service._lock_get_coordinate_info;
	        	}
	        	ngProgress.start();

	        	var deffer = $q.defer();
	        	$http.post($rootScope.server + '/api/coordinate/' + x + '/' + y + '/info/'
	        	).success(function(data) {
	        		deffer.resolve(data);
	        	}).error(function(data, status) {
	        		deffer.reject(data);
	        	}).finally(function() {
	        		service._lock_get_coordinate_info = null;
	        	});

	        	service._lock_get_coordinate_info = deffer.promise;
	        	return deffer.promise;
	        }

	        service.create_village = function(data, callback){
	        	ngProgress.start();
				var resp = $http.post($rootScope.server + '/api/create-village/', {
					'name': data.village_name,
					'location': data.location
				}).error(function(data, status) {
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	ngProgress.complete();
	            }).then(function(data){
	            	if(data.status == 200){
						service.get_villages();
	            	}
	            	return data;
	            });
	            return resp;
	        }

	        service.creating_map_info = function(callback){
	        	ngProgress.start();
				var resp = $http.get($rootScope.server + '/api/creating-map-info/'
				).error(function(data, status) {
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	ngProgress.complete();
	            }).then(function(data){
	            	if(data.status == 200){
	            		return data.data;
	            	}
	            	return null;
	            });
	            return resp;
	        }

	        service.get_buildings_data = function (callback) {
	        	if(service._lock_get_buildings_data){
	        		return service._lock_get_buildings_data;
	        	}
				ngProgress.start();
				var deferred = $q.defer();

				$http.post($rootScope.server + '/api/buildings-data/'
				).success(function(data) {
					$rootScope.globals.buildings = data.buildings;
	            	deferred.resolve(data);

	            	if($rootScope.globals.currentVillage.resources){
	            		$rootScope.globals.currentVillage.resources.max_population = CalcService.calc_prop('farm', 'max_population');
	            		$rootScope.globals.currentVillage.resources.storage_space = CalcService.calc_prop('storage', 'storage_space');
	            	}
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	service._lock_get_buildings_data = null;
	            	ngProgress.complete();
	            })
	            service._lock_get_buildings_data = deferred.promise;
	            return deferred.promise;
	        };

	        service.upgrade_building = function(building_type, village_id, stage, callback){
				if(service._lock_upgrade_building){
	        		return service._lock_upgrade_building;
	        	}
				ngProgress.start();
				var deferred = $q.defer();
				var url_path = '/api/building/upgrade/';
				if(stage == 0){
					var url_path = '/api/building/create/';
				}

				$http.post($rootScope.server + url_path, {
					'building_type': building_type,
					'village_id': village_id
				}).success(function(data) {
					if(!data.success){
						for(var i in data.errors){
							if(data.errors[i] == 2000){
								messageCenterService.add('info', 'Building is already upgrading!', {});
							} else if(data.errors[i] == 2001){
								messageCenterService.add('info', 'Building is already on max stage!', {});
							} else if(data.errors[i] == 2002){
								messageCenterService.add('info', 'Not enough resources!', {});
							} else if(data.errors[i] == 2003){
								messageCenterService.add('info', 'Village population is full!', {});
							}
						}
					} else {
						for(var i in $rootScope.globals.currentVillage.buildings){
							var building = $rootScope.globals.currentVillage.buildings[i];
							if(building.building_type == data.building_info.building.building_type){
								building.upgrading = true;
							}
						}
					}
	            	deferred.resolve(data);
	            	service.get_village_timers($rootScope.globals.currentVillage.id).then(function(data){ });
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            }).finally(function(){
	            	service._lock_upgrade_building = null;
	            	ngProgress.complete();
	            })
	            service._lock_upgrade_building = deferred.promise;
	            return deferred.promise;
	        }

	        service.start_training_units = function(data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/units/start-training/', data
				).success(function(data) {
					if(data.errors) {
						for(var i = 0; i < data.errors; i++){
							if(data.errors[i] == 2002) {
				            	messageCenterService.add('danger', 'Not enough resources!', {});
				            }
						}
					}
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.send_units = function(data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/units/expedition/', data
				).success(function(data) {
					if(data.success && data.success == true){
		            	messageCenterService.add('success', 'Units were sent!', {});
					} else if(data.errors) {
						for(var i = 0; i < data.errors; i++){
							if(data.errors[i] == 3001) {
				            	messageCenterService.add('danger', 'Units not avaliable!', {});
				            }
						}
					}

            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.get_reports = function(page_num, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/reports/' + page_num + '/'
				).success(function(data) {            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.get_report = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/report/' + id + '/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.create_market_offer = function(data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/market/offer/create/', data
				).success(function(data) {
					if(data.success && data.success == true){
		            	messageCenterService.add('success', 'Offer created!', {});
					}
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.get_market_offers = function(type, callback){
	        	var id = $rootScope.globals.currentVillage.id;
	        	url_postfix = '';
	        	if(type == 'my') {
	        		url_postfix = 'my/'
	        	}
	        	var deferred = $q.defer();
	        	if(!$rootScope.globals.currentVillage.id) {
	        		deferred.reject();
	        		return deferred.promise;
	        	}
				$http.get($rootScope.server + '/api/market/offers/' + id + '/' + url_postfix
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.accept_market_offer = function(id, callback){
	        	var data = { village_id: $rootScope.globals.currentVillage.id };

	        	var deferred = $q.defer();
	        	if(!$rootScope.globals.currentVillage.id) {
	        		deferred.reject();
	        		return deferred.promise;
	        	}
				$http.post($rootScope.server + '/api/market/offer/' + id + '/accept/', data
				).success(function(data) {
					if(data.error) {
						if(data.error == 2002){
			            	messageCenterService.add('danger', 'Not enough resources!', {});
						} else {
			            	messageCenterService.add('danger', 'Market offer already taken!', {});
						}
					}
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }


	        service.get_messages = function(m_type, page_num, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/messages/' + m_type + '/' + page_num + '/'
				).success(function(data) {
					deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.get_message = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/message/' + id + '/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.send_message = function(data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/message/send/', data
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.get_unread_badges = function(callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/info/badges/'
				).success(function(data) {
					$rootScope.globals.currentUser.badges = data;
					$rootScope.globals.currentUser.badges.last_update = Math.ceil(new Date().getTime() / 1000);
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }


	        service.get_player_info = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/player/' + id + '/info/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					deferred.reject();
	            	messageCenterService.add('danger', 'Something went wrong!', {});
	            })
	            return deferred.promise;
	        }

	        service.create_clan = function(data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/clan/create/', data
				).success(function(data) {
	            	messageCenterService.add('success', 'Clan created!', {});
            		deferred.resolve(data);
				}).error(function(data, status) {
					if(data.error == 5000) {
	            		messageCenterService.add('danger', 'Clan name alreay used!', {});
					} else if(data.error == 5001){
	            		messageCenterService.add('danger', 'Embassy is required!', {});
					} else {
	            		messageCenterService.add('danger', 'Something went wrong!', {});
	            	}
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.edit_clan = function(id, data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/clan/' + id + '/edit/', data
				).success(function(data) {
	            	messageCenterService.add('success', 'Clan saved!', {});
            		deferred.resolve(data);
				}).error(function(data, status) {
					if(data.error == 5000) {
	            		messageCenterService.add('danger', 'Clan name alreay used!', {});
					} else if(data.error == 5001){
	            		messageCenterService.add('danger', 'Embassy is required!', {});
					} else {
	            		messageCenterService.add('danger', 'Something went wrong!', {});
	            	}
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.leave_clan = function(clan_id, id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/clan/' + clan_id + '/player/' + id + '/leave/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.invite_to_clan = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/clan/invite/' + id + '/'
				).success(function(data) {
	            	messageCenterService.add('success', 'Player invited!', {});
            		deferred.resolve(data);
				}).error(function(data, status) {
					if(data.error == 5001) {
	            		messageCenterService.add('danger', 'Embassy is required!', {});
					} else if(data.error == 5003) {
	            		messageCenterService.add('danger', 'Clan is full or invites are not avaliable!', {});
					} else {
            			messageCenterService.add('danger', 'Something went wrong!', {});
            		}
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.has_clan_invitation = function(clan_id, id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/clan/' + clan_id + '/has-player-invitation/' + id + '/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.get_player_clan_invitations = function(callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/player/clan-invitations/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.get_clan_invitations = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/clan/' + id + '/invitations/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.accept_clan_invite = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/clan/' + id + '/accept-invite/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					console.log(status)
					if(status == 404) {
            			messageCenterService.add('danger', 'Invite is not avaliable!', {});
					} else if(data.error == 5002) {
            			messageCenterService.add('danger', 'You already have clan!', {});
					} else {
            			messageCenterService.add('danger', 'Something went wrong!', {});
            		}
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.remove_invite = function(clan_id, id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/clan/' + clan_id + '/remove-invite/' + id + '/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.get_clan_info = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/clan/' + id + '/info/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.get_topics = function(callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/forum/topics/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.get_topic = function(id, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/forum/topic/' + id + '/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.add_topic = function(data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/forum/topic/create/', data
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
					if(data.error == 6001) {
						messageCenterService.add('danger', 'Missing topic title!', {});
					} else if(data.error == 6000) {
						messageCenterService.add('danger', 'Topic already exist!', {});
					}else {
						messageCenterService.add('danger', 'Something went wrong!', {});
					}
            		
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.get_posts = function(topic_id, page, callback){
	        	var deferred = $q.defer();
				$http.get($rootScope.server + '/api/forum/topic/' + topic_id + '/posts/' + page + '/'
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        service.write_post = function(topic_id, data, callback){
	        	var deferred = $q.defer();
				$http.post($rootScope.server + '/api/forum/topic/' + topic_id + '/post/create/', data
				).success(function(data) {
            		deferred.resolve(data);
				}).error(function(data, status) {
            		messageCenterService.add('danger', 'Something went wrong!', {});
					deferred.reject();
	            })
	            return deferred.promise;
	        }

	        return service;
	}]);
})();