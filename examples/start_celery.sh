#!/bin/bash

source ~/env/bin/activate
cd ~/files

if [ "$1" == "default" ]; then
	celery worker --app=game -Q default
fi

#
if [ "$1" == "buildings" ]; then
	celery worker --app=game -B
fi 