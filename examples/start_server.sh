#!/bin/bash

source ~/env/bin/activate
cd ~/files

if [ "$1" == "f" ]; then
	python manage.py runserver 0.0.0.0:8000 --settings=game.settings.dev
else
    if [ "$1" == "m" ]; then
        python manage.py makemigrations game --settings=game.settings.dev
    fi

    python manage.py migrate --settings=game.settings.dev
    python manage.py runserver 0.0.0.0:8000 --settings=game.settings.dev
fi
