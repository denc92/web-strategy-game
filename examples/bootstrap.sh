#!/bin/bash
sudo locale-gen UTF-8

#set default mysql-server password to 'password' (last argument)
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'

echo "nameserver 8.8.8.8" | sudo tee /etc/resolv.conf

sudo apt-get update

sudo apt-get install --force-yes python python3-dev python-pip python-setuptools mysql-server libmysqlclient-dev python-dev build-essential python-dev libmysqlclient-dev subversion dos2unix python-software-properties python g++ make git rabbitmq-server
sudo pip install --upgrade virtualenv

#apache
sudo aptitude install apache2 apache2.2-common apache2-mpm-prefork apache2-utils libexpat1 ssl-cert
sudo aptitude install libapache2-mod-wsgi libapache2-mod-wsgi-py3
#sudo apt-get install -y apache2
sudo a2enmod proxy
sudo a2enmod rewrite
sudo a2enmod proxy_http
#sudo cp ~/files/examples/000-default.conf /etc/apache2/sites-available/000-default.conf
#sudo service apache2 restart

#import database
if ! mysql -u root -ppassword -e "USE 'game'"; then
	mysql -u root -ppassword -e 'drop database if exists game; create database game charset=utf8;'
	mysql -u root -ppassword game < ~/files/examples/database.sql --default-character-set=utf8
else
	echo "database already exists; skip it"
fi

#create virtualenv
virtualenv -p /usr/bin/python3.4 env
source ~/env/bin/activate

pip install -r ~/files/examples/requirements.txt