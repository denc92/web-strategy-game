Game App Backend
=======================

Prerequirements
===============

* [Vagrant](http://www.vagrantup.com/)

Set-up
======
    $ git clone ...
    $ vagrant up
	$ vagrant ssh

    #install required packages
	(vagrant)$ ./files/examples/bootstrap.sh


Development
===========

Django/Apache server can be started with:

	$ vagrant up
	$ vagrant ssh
	(vagrant)$ ~/files/examples/start_server.sh


	(vagrant)$ ~/files/examples/start_celery.sh default
	(vagrant)$ ~/files/examples/start_celery.sh buildings