from django.conf.urls import patterns, include, url
from django.contrib import admin
from game import views

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    # Auth
    url(r'^api/login/$', views.login),
    url(r'^api/register/$', views.register),

    # public APIs
    url(r'^api/buildings-data/$', views.buildings_data),
    url(r'^api/server-time/$', views.server_time),

    # Village
    url(r'^api/villages/$', views.villages),
    url(r'^api/full-village-data/(?P<id>[0-9]+)/$', views.full_village_data),
    url(r'^api/create-village/$', views.create_village),
    url(r'^api/creating-map-info/$', views.creating_map_info),
    url(r'^api/village/(?P<id>[0-9]+)/timers/$', views.village_timers),
    url(r'^api/map/(?P<x>-?\d+)/(?P<y>-?\d+)/$', views.generate_map),
    url(r'^api/coordinate/(?P<x>-?\d+)/(?P<y>-?\d+)/info/$', views.coordinate_info),

    # Building
    url(r'^api/building/create/$', views.create_building),
    url(r'^api/building/upgrade/$', views.upgrade_building),

    # Units
    url(r'^api/units/start-training/$', views.units_start_training),
    url(r'^api/units/expedition/$', views.units_expedition),

    # reports
    url(r'^api/reports/$', views.reports),
    url(r'^api/reports/(?P<page>[0-9]+)/$', views.reports),
    url(r'^api/report/(?P<id>[0-9]+)/$', views.report),

    # market
    url(r'^api/market/offers/(?P<id>[0-9]+)/$', views.market_get_offers),
    url(r'^api/market/offers/(?P<id>[0-9]+)/my/$', views.market_get_offers),
    url(r'^api/market/offer/create/$', views.market_create_offer),
    url(r'^api/market/offer/(?P<id>[0-9]+)/accept/$', views.accept_market_offer),

    # messages
    url(r'^api/messages/(?P<m_type>\w+)/(?P<page>[0-9]+)/$', views.messages),
    url(r'^api/message/send/$', views.message_send),
    url(r'^api/message/(?P<id>[0-9]+)/$', views.get_message),

    # status
    url(r'^api/info/badges/', views.info_badges),

    # clan
    url(r'^api/clan/create/', views.create_clan),
    url(r'^api/clan/(?P<clan_id>[0-9]+)/player/(?P<id>[0-9]+)/leave/',
        views.leave_clan),
    url(r'^api/clan/invite/(?P<id>[0-9]+)/$', views.send_clan_invite),
    url(r'^api/clan/(?P<clan_id>[0-9]+)/remove-invite/(?P<id>[0-9]+)/',
        views.remove_clan_invite),
    url(r'^api/clan/(?P<id>[0-9]+)/invitations/', views.get_clan_invitations),
    url(r'^api/clan/(?P<id>[0-9]+)/info/', views.get_clan_info),
    url(r'^api/clan/(?P<id>[0-9]+)/edit/', views.edit_clan),
    url(r'^api/clan/(?P<clan_id>[0-9]+)/has-player-invitation/(?P<id>[0-9]+)/$',
        views.has_clan_invitation),
    url(r'^api/clan/(?P<id>[0-9]+)/accept-invite/', views.accept_clan_invite),


    # player
    url(r'^api/player/(?P<id>[0-9]+)/info/$', views.get_player_info),
    url(r'^api/player/clan-invitations/$', views.get_player_clan_invitations),

    # forum
    url(r'^api/forum/topic/create/$', views.add_forum_topic),
    url(r'^api/forum/topics/$', views.get_forum_topics),
    url(r'^api/forum/topic/(?P<id>[0-9]+)/$', views.get_topic),
    url(r'^api/forum/topic/(?P<id>[0-9]+)/post/create/$',
        views.create_topic_post),
    url(r'^api/forum/topic/(?P<topic_id>[0-9]+)/posts/(?P<page>[0-9]+)/$',
        views.get_topic_posts),
)
