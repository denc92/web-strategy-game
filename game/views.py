# -*- coding: utf-8 -*-
import json
from datetime import datetime
from django.shortcuts import HttpResponse
from django.contrib.auth import authenticate
from django.utils import tzinfo, timezone
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Q
import game.status_codes as code
from game.models import LoginToken, Village, MapCoordinate, Building, Report,\
    MarketOffer, Message, UserProfile, Clan, ClanInvite, Forum, ForumTopic,\
    TopicPost, ForumTopicLastSeen


def superuser_required(func):
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated()\
                or not request.user.is_superuser:
            return HttpResponse(status=403)
        return func(request, *args, **kwargs)
    return wrapper


def token_required(func):
    def wrapper(request, *args, **kwargs):
        if 'HTTP_AUTHORIZATION' not in request.META:
            return HttpResponse(status=400)

        auth = request.META['HTTP_AUTHORIZATION'].split(' ')
        try:
            token = LoginToken.objects.select_related(
                'user').get(token=auth[1])
        except LoginToken.DoesNotExist:
            return HttpResponse(status=401)

        request.user = token.user
        return func(request, *args, **kwargs)
    return wrapper


@csrf_exempt
def login(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'username' not in data or 'password' not in data:
        return HttpResponse(status=400)

    user = authenticate(username=data['username'], password=data['password'])
    if not user:
        return HttpResponse(status=403)

    token = LoginToken.create_token(user)
    resp = {
        'user_id': user.id,
        'token': token.token
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
def register(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'username' not in data or 'password' not in data\
            or 'email' not in data:
        return HttpResponse(status=400)

    resp = {
        'errors': [],
        'success': True
    }

    if User.objects.filter(username=data['username']).count() > 0:
        resp['errors'].append(code.USERNAME_ALREADY_USED)
    if User.objects.filter(username=data['email']).count() > 0:
        resp['errors'].append(code.EMAIL_ALREADY_USED)

    if len(resp['errors']) > 0:
        resp['success'] = False

    if resp['success']:
        user = User.objects.create_user(
            data['username'], data['email'], data['password']
        )
        UserProfile.objects.create(
            user=user
        )

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
def server_time(request):
    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    resp = {
        'server_time': now.timestamp()
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')

@csrf_exempt
def buildings_data(request):
    resp = {
        'buildings': settings.BUILDINGS
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def full_village_data(request, id):
    try:
        village = Village.objects.prefetch_related(
            'building_set', 'armytraining_set').get(owner=request.user, id=id)
    except Village.DoesNotExist:
        return HttpResponse(status=404)

    resp = {
        'village': village.load_village()
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def get_player_info(request, id):
    try:
        user = UserProfile.objects.select_related(
            'user', 'clan', 'clan__leader').get(user__id=id)
    except UserProfile.DoesNotExist:
        return HttpResponse(status=404)

    return HttpResponse(json.dumps(user.to_dict(), cls=DjangoJSONEncoder),
                        content_type='application/json')

@csrf_exempt
@token_required
def villages(request):
    resp = {
        'villages': Village.get_villages(request.user)
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def creating_map_info(request):
    resp = {
        'ne': MapCoordinate.objects.filter(x__gte=0, y__gte=0, village__isnull=False).count(),
        'nw': MapCoordinate.objects.filter(x__lt=0, y__gte=0, village__isnull=False).count(),
        'se': MapCoordinate.objects.filter(x__gte=0, y__lt=0, village__isnull=False).count(),
        'sw': MapCoordinate.objects.filter(x__lt=0, y__lt=0, village__isnull=False).count(),
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def create_village(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'name' not in data or 'location' not in data:
        return HttpResponse(status=400)

    if Village.objects.filter(owner=request.user).count() > 0:
        return HttpResponse(status=403)

    resp = {
        'village': Village.create_village(
            request.user, data['name'], data['location'])
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def create_building(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'building_type' not in data or 'village_id' not in data:
        return HttpResponse(status=400)

    try:
        building = Building.objects.select_related('village').get(
            building_type=data['building_type'],
            village__id=data['village_id'],
            village__owner=request.user
        )
    except Building.DoesNotExist:
        building = None

    if building:
        # already exist
        return HttpResponse(status=400)

    try:
        village = Village.objects.get(id=data['village_id'])
    except Village.DoesNotExist:
        return HttpResponse(status=404)

    queue = Building.create_building(village, data['building_type'])
    resp = {
        "success": False
    }
    if type(queue) != int:
        resp = {
            "success": True,
            "building_info": queue.to_dict()
        }
    else:
        resp['errors'] = [queue]

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def upgrade_building(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'building_type' not in data or 'village_id' not in data:
        return HttpResponse(status=400)

    try:
        building = Building.objects.select_related('village').get(
            building_type=data['building_type'],
            village__id=data['village_id'],
            village__owner=request.user
        )
    except Building.DoesNotExist:
        return HttpResponse(status=404)

    queue = building.upgrade_building()
    resp = {
        "success": False
    }
    if type(queue) != int:
        resp = {
            "success": True,
            "building_info": queue.to_dict()
        }
    else:
        resp['errors'] = [queue]

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def village_timers(request, id):
    try:
        village = Village.objects.get(owner=request.user, id=id)
    except Village.DoesNotExist:
        return HttpResponse(status=404)

    resp = {
        'buildings_upgrades': village.get_upgrade_timers(),
        'army_movement': village.get_army_movement(),
        'market_movement': village.get_market_movement()
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def generate_map(request, x, y):
    try:
        mapCoordinate = MapCoordinate.objects.get(x=x, y=y)
    except MapCoordinate.DoesNotExist:
        return HttpResponse(status=404)

    resp = {
        'map': mapCoordinate.get_map(request.user)
    }
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def coordinate_info(request, x, y):
    try:
        mapCoordinate = MapCoordinate.objects.get(x=x, y=y)
    except MapCoordinate.DoesNotExist:
        return HttpResponse(status=404)

    resp = mapCoordinate.to_dict(request.user)
    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def units_start_training(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'village_id' not in data or 'units' not in data:
        return HttpResponse(status=400)

    try:
        barracks = Building.objects.select_related('village').get(
            village__id=data['village_id'],
            village__owner=request.user,
            building_type='barracks',
            stage__gt=0
        )
    except Building.DoesNotExist:
        return HttpResponse(status=404)

    queue = barracks.start_training(data['units'])

    if queue is None:
        return HttpResponse(400)

    resp = {
        "success": False
    }
    if type(queue) != int:
        resp = {
            "success": True,
            "units_training": barracks.village.get_units_trainings()
        }
    else:
        resp['errors'] = [queue]

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def units_expedition(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'village_id' not in data or 'target_village_id' not in data or\
            'units' not in data or 'type' not in data:
        return HttpResponse(status=400)

    try:
        village = Village.objects.get(
            owner=request.user, id=data['village_id'])
    except Village.DoesNotExist:
        return HttpResponse(status=404)

    try:
        target_village = Village.objects.get(id=data['target_village_id'])
    except Village.DoesNotExist:
        return HttpResponse(status=404)

    res = village.send_army(target_village, data['units'], data['type'])

    resp = {
        "success": False
    }
    if type(res) != int:
        resp = {
            "success": True,
            "army_movements": village.get_army_movement()
        }
    else:
        resp['errors'] = [res]

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def reports(request, page=1):
    page = int(page)
    start = (page - 1) * settings.REPORTS['page_size']
    end = page * settings.REPORTS['page_size']
    reports = Report.objects.select_related(
        'start_village', 'start_village__owner',
        'end_village', 'end_village__owner').filter(
        user=request.user
    ).order_by('-added_at').all()[start:end]

    resp = {
        'reports': [],
        'num_of_reports': Report.objects.filter(user=request.user).count(),
        'page_size': settings.REPORTS['page_size']
    }
    for report in reports:
        resp['reports'].append(report.to_dict())

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def report(request, id):
    try:
        report = Report.objects.get(id=id, user=request.user)
    except Report.DoesNotExist:
        return HttpResponse(status=404)

    if report.seen is None:
        report.seen = datetime.utcnow().replace(tzinfo=timezone.utc)
        report.save()

    return HttpResponse(json.dumps(report.to_dict(), cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def market_create_offer(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'offered' not in data or 'value' not in data['offered'] or\
            'resource' not in data['offered'] or 'requested' not in data or\
            'value' not in data['requested'] or 'village_id' not in data:
        return HttpResponse(status=400)

    try:
        market = Building.objects.select_related('village').get(
            village__id=data['village_id'],
            village__owner=request.user,
            building_type='market',
            stage__gt=0
        )
    except Building.DoesNotExist:
        return HttpResponse(status=404)

    offer = MarketOffer.create_offer(
        market, data['offered'], data['requested'])

    if offer is None:
        return HttpResponse(status=400)
    return HttpResponse(json.dumps(offer.to_dict(), cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def market_get_offers(request, id):
    try:
        village = Village.objects.get(id=id, owner=request.user)
    except Village.DoesNotExist:
        return HttpResponse(status=404)

    resp = {}
    if request.path.endswith('/my/'):
        resp['offers'] = MarketOffer.get_offers(village, True)
        resp['used_traders'] = MarketOffer.get_num_of_used_traders(village)
    else:
        resp['offers'] = MarketOffer.get_offers(village)



    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def accept_market_offer(request, id):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:

        return HttpResponse(status=400)

    if 'village_id' not in data:
        return HttpResponse(status=400)

    try:
        offer = MarketOffer.objects.select_related('owner_village').get(id=id)
    except MarketOffer.DoesNotExist:
        return HttpResponse(status=404)

    try:
        market = Building.objects.select_related('village').get(
            village__id=data['village_id'],
            village__owner=request.user,
            building_type='market',
            stage__gt=0
        )
    except Building.DoesNotExist:
        return HttpResponse(status=404)

    r = offer.accept_offer(market.village)
    if 'error' in r:
        return HttpResponse(
            json.dumps(r), status=400, content_type='application/json')
    return HttpResponse(json.dumps(r), content_type='application/json')


@csrf_exempt
@token_required
def messages(request, m_type, page):
    page = int(page)
    start = (page - 1) * settings.MESSAGES['page_size']
    end = page * settings.MESSAGES['page_size']

    query = Q(receiver=request.user)
    if m_type == 'sent':
        query = Q(sender=request.user)

    messages = Message.objects.select_related('sender', 'receiver')\
        .filter(query).order_by('-added_at').all()[start:end]

    resp = {
        'messages': [],
        'num_of_messages': Message.objects.filter(query).count(),
        'page_size': settings.MESSAGES['page_size']
    }
    for message in messages:
        resp['messages'].append(message.to_dict())

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def message_send(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'receiver_id' not in data or 'content' not in data or\
            'subject' not in data:
        return HttpResponse(status=400)

    try:
        receiver = User.objects.get(id=data['receiver_id'])
    except User.DoesNotExist:
        return HttpResponse(status=404)

    Message.create_message(
        request.user, receiver, data['subject'], data['content'])
    return HttpResponse(status=200)


@csrf_exempt
@token_required
def get_message(request, id):
    try:
        query = Q(id=id) & (Q(sender=request.user) | Q(receiver=request.user))
        message = Message.objects.get(query)
    except Message.DoesNotExist:
        return HttpResponse(status=404)

    message.seen = datetime.utcnow().replace(tzinfo=timezone.utc)
    if request.user == message.receiver:
        message.save()

    return HttpResponse(json.dumps(message.to_dict(), cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def info_badges(request):
    resp = {
        'reports': Report.get_unread_badge(request.user),
        'messages': Message.get_unread_badge(request.user),
        'forum': Forum.get_unread_badge(request.user)
    }

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def create_clan(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'name' not in data or request.user.userprofile.clan is not None:
        return HttpResponse(status=400)

    if Clan.objects.filter(name__iexact=data['name']).exists():
        return HttpResponse(
            json.dumps({'error': code.CLAN_NAME_ALREADY_EXIST}),
            status=400, content_type='application/json'
        )

    try:
        embassy = Building.objects.get(
            village__owner=request.user,
            building_type='embassy',
            stage__gt=0
        )
    except Building.DoesNotExist:
        return HttpResponse(
            json.dumps({'error': code.EMBASSY_IS_REQUIRED}),
            status=400, content_type='application/json'
        )

    clan = Clan.objects.create(
        name=data['name'],
        leader=request.user,
        description=data['description'] if 'description' in data else None
    )

    forum = Forum.objects.create(clan=clan)
    request.user.userprofile.clan = clan
    request.user.userprofile.save()

    return HttpResponse(json.dumps(clan.to_dict()))


@csrf_exempt
@token_required
def edit_clan(request, id):
    try:
        clan = Clan.objects.get(id=id)
    except Clan.DoesNotExist:
        return HttpResponse(status=404)

    if clan.leader_id != request.user.id:
        return HttpResponse(status=401)

    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'name' not in data:
        return HttpResponse(status=400)

    if Clan.objects.filter(name=data['name']).exclude(id=clan.id).exists():
        return HttpResponse(
            json.dumps({'error': code.CLAN_NAME_ALREADY_EXIST}),
            status=400, content_type='application/json'
        )

    clan.name = data['name']
    clan.description = None
    if 'description' in data:
        clan.description = data['description']

    clan.save()

    return HttpResponse(json.dumps(clan.to_dict()))


@csrf_exempt
@token_required
def leave_clan(request, clan_id, id):
    if int(request.user.id) == int(id):
        request.user.userprofile.leave_clan()
        return HttpResponse()

    try:
        user = UserProfile.objects.get(clan__id=clan_id, user__id=id)
    except UserProfile.DoesNotExist:
        return HttpResponse(status=400)

    if user.clan != request.user.userprofile.clan or\
            request.user.userprofile.clan.leader != request.user:
        return HttpResponse(status=401)
    user.leave_clan()

    return HttpResponse()


@csrf_exempt
@token_required
def has_clan_invitation(request, clan_id, id):
    try:
        clan = Clan.objects.get(id=clan_id)
    except Clan.DoesNotExist:
        return HttpResponse(status=404)

    if clan.leader != request.user:
        return HttpResponse(status=401)

    resp = {
        'exist': ClanInvite.objects.filter(clan=clan, user__id=id).exists()
    }
    return HttpResponse(json.dumps(resp), content_type='application/json')


@csrf_exempt
@token_required
def send_clan_invite(request, id):
    clan = request.user.userprofile.clan
    if clan is None or clan.leader != request.user:
        return HttpResponse(status=401)


    embassy = Building.objects.filter(
        village__owner=request.user,
        building_type='embassy'
    ).order_by('-stage').first()

    if not embassy:
        return HttpResponse(
            json.dumps({'error': code.EMBASSY_IS_REQUIRED}),
            status=400, content_type='application/json'
        )

    max_members = embassy.calc_prop('members')
    current_members = UserProfile.objects.filter(clan=clan).count()
    current_invites = ClanInvite.objects.filter(clan=clan).count()
    if max_members <= current_invites + current_members:
        return HttpResponse(
            json.dumps({'error': code.CLAN_IS_FULL}),
            status=400, content_type='application/json'
        )

    try:
        user = User.objects.get(id=id)
    except User.DoesNotExist:
        return HttpResponse(status=404)

    if not ClanInvite.objects.filter(clan=clan, user=user).exists():
        ClanInvite.objects.create(
            clan=clan,
            user=user
        )

    return HttpResponse(json.dumps({'success': True}),
                        content_type='application/json')


@csrf_exempt
@token_required
def get_clan_invitations(request, id):
    if request.user.userprofile.clan is None or\
            request.user.userprofile.clan.id != int(id) or\
            request.user.userprofile.clan.leader != request.user:
        return HttpResponse(status=401)

    invitations = ClanInvite.get_clan_invitations(
        request.user.userprofile.clan)
    return HttpResponse(
        json.dumps(invitations, cls=DjangoJSONEncoder),
        content_type='application/json'
    )


@csrf_exempt
@token_required
def get_player_clan_invitations(request):
    invitations = ClanInvite.get_invitations(request.user)
    return HttpResponse(
        json.dumps(invitations, cls=DjangoJSONEncoder),
        content_type='application/json'
    )


@csrf_exempt
@token_required
def accept_clan_invite(request, id):
    try:
        invite = ClanInvite.objects.get(clan_id=id, user=request.user)
    except ClanInvite.DoesNotExist:
        return HttpResponse(status=404)

    if request.user.userprofile.clan is not None:
        return HttpResponse(json.dumps({'error': code.ALREADY_HAVE_CLAN}),
                            content_type='application/json')

    request.user.userprofile.clan = invite.clan
    request.user.userprofile.save()

    invite.delete()
    return HttpResponse(json.dumps({'success': True}),
                        content_type='application/json')


@csrf_exempt
@token_required
def remove_clan_invite(request, clan_id, id):
    if request.user.userprofile.clan is None or\
            request.user.userprofile.clan.id != int(clan_id) or\
            request.user.userprofile.clan.leader != request.user:
        return HttpResponse(status=401)

    try:
        invite = ClanInvite.objects.get(clan_id=clan_id, id=id)
    except ClanInvite.DoesNotExist:
        return HttpResponse(status=404)

    invite.delete()
    return HttpResponse(json.dumps({'success': True}),
                        content_type='application/json')


@csrf_exempt
@token_required
def get_clan_info(request, id):
    try:
        clan = Clan.objects.select_related('leader').get(id=id)
    except Clan.DoesNotExist:
        return HttpResponse(status=404)

    return HttpResponse(json.dumps(clan.to_dict(full_info=True)),
                        content_type='application/json')


@csrf_exempt
@token_required
def add_forum_topic(request):
    if not request.user.userprofile.is_clan_leader():
        return HttpResponse(status=401)

    forum = request.user.userprofile.get_clan_forum()
    if not forum:
        return HttpResponse(status=404)

    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    if 'title' not in data:
        return HttpResponse(json.dumps({'error': code.MISSING_TOPIC_TITLE}),
                            content_type='application/json')

    if ForumTopic.objects.filter(
            forum=forum, title__iexact=data['title']).exists():
        return HttpResponse(json.dumps({'error': code.TOPIC_ALREADY_EXIST}),
                            content_type='application/json')

    topic = ForumTopic.objects.create(
        forum=forum,
        title=data['title']
    )

    return HttpResponse(json.dumps(topic.to_dict()),
                        content_type='application/json')


@csrf_exempt
@token_required
def get_forum_topics(request):
    forum = request.user.userprofile.get_clan_forum()
    if not forum:
        return HttpResponse(status=404)

    return HttpResponse(json.dumps(forum.get_topics(request.user)),
                        content_type='application/json')


@csrf_exempt
@token_required
def get_topic(request, id):
    forum = request.user.userprofile.get_clan_forum()
    if not forum:
        return HttpResponse(status=404)

    try:
        topic = ForumTopic.objects.get(forum=forum, id=id)
    except ForumTopic.DoesNotExist:
        return HttpResponse(status=404)

    resp = topic.to_dict(request.user)

    last_seen = ForumTopicLastSeen.objects.filter(
        topic=topic, user=request.user).first()

    if not last_seen:
        last_seen = ForumTopicLastSeen()
        last_seen.topic = topic
        last_seen.user = request.user
    last_seen.last_seen = datetime.utcnow().replace(tzinfo=timezone.utc)
    last_seen.save()

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def get_topic_posts(request, topic_id, page):
    forum = request.user.userprofile.get_clan_forum()
    if not forum:
        return HttpResponse(status=404)

    try:
        topic = ForumTopic.objects.get(forum=forum, id=topic_id)
    except ForumTopic.DoesNotExist:
        return HttpResponse(status=404)

    page = int(page)
    start = (page - 1) * settings.FORUM['post_per_page']
    end = page * settings.FORUM['post_per_page']

    posts = TopicPost.objects.select_related('user').filter(topic=topic)\
        .order_by('-added_at').all()[start:end]

    resp = {
        'posts': [],
        'num_of_posts': TopicPost.objects.filter(topic=topic).count(),
        'page_size': settings.FORUM['post_per_page']
    }
    for post in posts:
        resp['posts'].append(post.to_dict())

    return HttpResponse(json.dumps(resp, cls=DjangoJSONEncoder),
                        content_type='application/json')


@csrf_exempt
@token_required
def create_topic_post(request, id):
    forum = request.user.userprofile.get_clan_forum()
    if not forum:
        return HttpResponse(status=404)

    try:
        data = json.loads(request.body.decode('utf-8'))
    except:
        return HttpResponse(status=400)

    try:
        topic = ForumTopic.objects.get(forum=forum, id=id)
    except ForumTopic.DoesNotExist:
        return HttpResponse(status=404)

    post = TopicPost.objects.create(
        topic=topic,
        user=request.user,
        content=data['content']
    )

    return HttpResponse(json.dumps(post.to_dict(), cls=DjangoJSONEncoder),
                        content_type='application/json')
