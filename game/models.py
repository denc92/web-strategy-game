# -*- coding: utf-8 -*-
import string
import random
import math
import time
import json
from datetime import datetime, timedelta
from polymorphic import PolymorphicModel
from django.utils import timezone
from django.db import models, connection
from django.db.models import Q, Prefetch
from django.contrib.auth.models import User
from django.conf import settings
import game.status_codes as code
from game.tasks import generate_map_image
from game.functions import get_movement_speed, calc_travel_time,\
    split_carrying_capacity


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    clan = models.ForeignKey('Clan', null=True, blank=True, db_index=True)

    def is_clan_leader(self):
        if self.clan is None or self.clan.leader != self.user:
            return False
        return True

    def get_clan_forum(self):
        if self.clan is None:
            return None

        return Forum.objects.get(clan=self.clan)

    def to_dict(self):
        resp = {
            'id': self.user.id,
            'username': self.user.username,
            'clan': None,
            'villages': self.get_villages(),
            'total_population': 0
        }

        for v in resp['villages']:
            resp['total_population'] += v['population']

        if self.clan:
            resp['clan'] = {
                'id': self.clan.id,
                'name': self.clan.name,
                'leader': {
                    'id': self.clan.leader.id,
                    'username': self.clan.leader.username
                }
            }

        return resp

    def leave_clan(self):
        if self.clan and self.clan.leader == self.user:
            members = self.clan.get_members()
            if len(list(members)) == 1:
                self.clan.remove_all_clan_invites()
                self.clan.delete()
            else:
                for member in members:
                    if member != self.user:
                        self.clan.leader = member
                        self.clan.save()
                        break

        self.clan = None
        self.save()

    def get_villages(self):
        villages = []
        for mapC in MapCoordinate.objects.select_related('village')\
                .filter(village__owner=self.user)\
                .order_by('-village__village_population').all():
            villages.append({
                'x': mapC.x,
                'y': mapC.y,
                'village_id': mapC.village.id,
                'name': mapC.village.name,
                'population': mapC.village.village_population
            })

        return villages


class LoginToken(models.Model):
    user = models.ForeignKey(User)
    token = models.CharField(max_length=40)

    added_at = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def create_token(user):
        return LoginToken.objects.create(
            user=user,
            token=''.join(random.SystemRandom().choice(
                string.ascii_lowercase + string.digits) for _ in range(40)
            )
        )


class Village(models.Model):
    owner = models.ForeignKey(User, db_index=True)
    name = models.CharField(max_length=50)
    village_population = models.IntegerField(default=0)

    # resources
    stone = models.IntegerField(default=0)
    wood = models.IntegerField(default=0)
    clay = models.IntegerField(default=0)
    storage_space = models.IntegerField(default=1000)
    army_population = models.IntegerField(default=0)

    loyalty = models.SmallIntegerField(default=100)

    last_updated = models.DateTimeField(auto_now_add=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def update_village_resources(self):
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        seconds = (now - self.last_updated).total_seconds()

        for building in self.building_set.filter(
                building_type__in=['stone_mine', 'clay_pit', 'lumber_camp']
                ).all():
            income = building.calc_prop('income_speed')
            if building.building_type == 'stone_mine':
                self.stone = self.stone + int(income / 3600 * seconds)
            elif building.building_type == 'clay_pit':
                self.clay = self.clay + int(income / 3600 * seconds)
            elif building.building_type == 'lumber_camp':
                self.wood = self.wood + int(income / 3600 * seconds)


        if self.stone > self.storage_space:
            self.stone = self.storage_space
        if self.wood > self.storage_space:
            self.wood = self.storage_space
        if self.clay > self.storage_space:
            self.clay = self.storage_space

        self.last_updated = now
        self.save()

    def update_village_army_training(self):
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        trained_units = {}

        for at in self.armytraining_set.all():
            end_time = at.start_at + timedelta(seconds=at.training_time)
            if now > end_time:
                if at.unit_type not in trained_units:
                    trained_units[at.unit_type] = 0
                trained_units[at.unit_type] += at.number
                at.delete()
            elif now > at.start_at:
                # already trained
                one_unit_time = at.training_time / at.number
                old_diff = int((at.updated_time - at.start_at).total_seconds())
                diff = int((now - at.start_at).total_seconds())

                old_already_trained = math.floor(old_diff / one_unit_time)
                already_trained = math.floor(diff / one_unit_time)

                if already_trained > old_already_trained:
                    if at.unit_type not in trained_units:
                        trained_units[at.unit_type] = 0
                    trained_units[at.unit_type] += \
                        already_trained - old_already_trained
                at.updated_time = datetime.utcnow().replace(
                    tzinfo=timezone.utc)
                at.save()

        ArmyGroup.add_to_army_groups(self, trained_units)


    def population(self):
        return self.army_population + self.village_population

    def load_village(self):
        self.update_village_resources()
        self.update_village_army_training()
        buildings = self.get_buildings()
        coo = {}
        try:
            mapCoordinate = MapCoordinate.objects.get(village=self)
            coo = {
                'x': mapCoordinate.x,
                'y': mapCoordinate.y
            }
        except MapCoordinate.DoesNotExist:
            pass

        return {
            'id': self.id,
            'name': self.name,
            'resources': {
                'stone': self.stone,
                'wood': self.wood,
                'clay': self.clay,
                'population': self.population(),
                'max_stone': self.calc_prop('storage'),
                'max_wood': self.calc_prop('storage'),
                'max_clay': self.calc_prop('storage'),
                'max_population': self.calc_prop('farm'),
                'stone_income': self.calc_prop('stone_mine'),
                'wood_income': self.calc_prop('lumber_camp'),
                'clay_income': self.calc_prop('clay_pit')
            },
            'army': {
                'units': self.get_army(own=False),
                'units_training': self.get_units_trainings()
            },
            'coordinates': coo,
            'loyalty': self.loyalty,
            'buildings': buildings
        }

    def get_buildings(self):
        buildings = []
        for building in self.building_set.all():
            buildings.append(building.to_dict())

        return buildings

    def calc_prop(self, prop):
        for building in self.building_set.all():
            if prop == building.building_type:
                if prop in ['stone_mine', 'clay_pit', 'lumber_camp']:
                    return building.calc_prop('income_speed')
                elif prop == 'storage':
                    return building.calc_prop('storage_space')
                elif prop == 'farm':
                    return  building.calc_prop('max_population')
        return None

    def get_upgrade_timers(self):
        arr = []
        for ubq in UpgradeBuildingQueue.objects.filter(
                building__village=self).order_by('start_at').all():
            arr.append(ubq.to_dict())

        return arr

    def get_units_trainings(self):
        self.update_village_army_training()
        data = []
        army_trainings = self.armytraining_set.all()
        for at in army_trainings:
            data.append(at.to_dict())

        return data

    def get_army(self, own=False):
        return ArmyGroup.get_village_army(self, own=own)

    def send_army(self, target_village, units, expedition_type):
        return ArmyExpedition.create_expedition(
            self, target_village, units, expedition_type
        )

    def get_army_movement(self):
        return ArmyExpedition.get_army_movements(self)

    def get_market_movement(self):
        resp = []
        query = (~Q(status='W') & Q(owner_village=self)) |\
                (Q(status='D') & Q(end_village=self))

        for mo in MarketOffer.objects.filter(query).all():
            resp.append(mo.to_dict())
        return resp

    @staticmethod
    def get_villages(user):
        villages = []

        for village in Village.objects.filter(owner=user).order_by('id').all():
            villages.append({
                'id': village.id,
                'name': village.name,
                'village_population': village.village_population,
            })

        return villages

    @staticmethod
    def create_village(user, name, location):
        village = None
        query = Q(x__gte=0) & Q(y__gte=0)
        min_angle = 0
        max_angle = 90
        if location == 'nw':
            query = Q(x__lt=0) & Q(y__gte=0)
            min_angle = 90
            max_angle = 180
        if location == 'sw':
            query = Q(x__lt=0) & Q(y__lt=0)
            min_angle = 180
            max_angle = 270
        if location == 'se':
            query = Q(x__gte=0) & Q(y__lt=0)
            min_angle = 270
            max_angle = 360

        query = query & Q(village__isnull=False)

        villages = MapCoordinate.objects.filter(query).count()
        if villages == 0:
            villages = 1

        # 10% % of space are villages, * 4 for r
        v = villages * 10 * 4
        r = math.sqrt(v / math.pi)

        max_r = math.sqrt(math.pow(settings.DEFAULT_MAP_SETTINGS['max_x'], 2) +
                          math.pow(settings.DEFAULT_MAP_SETTINGS['max_y'], 2))

        counter = 0
        while True:
            counter += 1
            if counter == 100:
                counter = 0
                r += 1

            angle = random.randint(min_angle, max_angle)
            x = int(r * math.cos(math.radians(angle)))
            y = int(r * math.sin(math.radians(angle)))

            if r > max_r:
                break

            if x > settings.DEFAULT_MAP_SETTINGS['max_x'] or\
                    x < settings.DEFAULT_MAP_SETTINGS['min_x'] or\
                    y > settings.DEFAULT_MAP_SETTINGS['max_y'] or\
                    y < settings.DEFAULT_MAP_SETTINGS['min_y']:
                continue

            map_coordinate = MapCoordinate.objects.get(x=x, y=y)
            if map_coordinate.coordinate_type == 'G'\
                    and not map_coordinate.village:
                default = settings.DEFAULT_VILLAGE_SETTINGS
                village = Village.objects.create(
                    owner=user,
                    name=name,
                    stone=default['resources'],
                    wood=default['resources'],
                    clay=default['resources'],
                    village_population=default['village_population']
                )
                map_coordinate.village = village
                map_coordinate.save()
                Building.build_default_buildings(village)
                break

        # celery
        generate_map_image.delay({})

        return village.load_village()


class MapCoordinate(models.Model):
    x = models.SmallIntegerField()
    y = models.SmallIntegerField()
    village = models.ForeignKey(Village, null=True, blank=True)

    choices = (
        ('G', 'Grass'), ('S', 'Sea'), ('F', 'Forest'), ('M', 'Mountain')
    )
    coordinate_type = models.CharField(
        max_length=1, choices=choices, default='G')

    def get_map(self, user):
        sett = settings.DEFAULT_MAP_SETTINGS
        s_r = int(sett['screen_size'] / 2)

        query = None
        if self.x - s_r < sett['min_x']:
            query = Q(x__gte=sett['min_x']) &\
                    Q(x__lte=sett['min_x'] + sett['screen_size'])
        elif self.x + s_r > sett['max_x']:
            query = Q(x__gte=sett['max_x'] - sett['screen_size']) &\
                    Q(x__lte=sett['max_x'])
        else:
            query = Q(x__gte=self.x - s_r) & Q(x__lte=self.x + s_r)

        if self.y - s_r < sett['min_y']:
            query = query & (Q(y__gte=sett['min_y']) &
                             Q(y__lte=sett['min_y'] + sett['screen_size']))
        elif self.y + s_r > sett['max_y']:
            query = query & (Q(y__gte=sett['max_y'] - sett['screen_size']) &
                             Q(y__lte=sett['max_y']))
        else:
            query = query & (Q(y__gte=self.y - s_r) & Q(y__lte=self.y + s_r))

        map_coordinates = MapCoordinate.objects.select_related(
            'village', 'village__owner', 'village__owner__userprofile'
        ).filter(query).all().order_by('-y', 'x')

        map_a = []
        for mc in map_coordinates:
            map_a.append(mc.to_dict(user))

        return map_a

    def to_dict(self, user=None):
        mc = {
            'x': self.x,
            'y': self.y,
            'type': self.coordinate_type,
            'village': None
        }
        if self.village:
            clan = None
            relation = 'enemy'
            if user == self.village.owner:
                relation = 'owner'
            elif user.userprofile.clan_id ==\
                    self.village.owner.userprofile.clan_id and\
                    self.village.owner.userprofile.clan != None:
                relation = 'clan_member'

            if self.village.owner.userprofile.clan_id:
                clan = {
                    'id': self.village.owner.userprofile.clan.id,
                    'name': self.village.owner.userprofile.clan.name
                }

            mc['village'] = {
                'player': {
                    'id': self.village.owner.id,
                    'username': self.village.owner.username,
                    'relation': relation,
                    'clan': clan
                },
                'village_id': self.village.id,
                'name': self.village.name,
                'population': self.village.village_population
            }

        return mc


class Building(models.Model):
    village = models.ForeignKey(Village, db_index=True)
    choices = (
        ('castle', 'Castle'),
        ('stone_mine', 'Stone mine'),
        ('clay_pit', 'Clay pit'),
        ('lumber_camp', 'Lumber camp'),
        ('farm', 'Farm'),
        ('barracks', 'Barracks')
    )
    building_type = models.CharField(
        max_length=20, choices=choices, default='castle', db_index=True)
    stage = models.SmallIntegerField(default=0)
    upgrading = models.BooleanField(default=False)

    def get_stats(self):
        stats = {}
        if self.building_type in ['stone_mine', 'clay_pit', 'lumber_camp']:
            stats['income'] = self.calc_prop('income_speed')
        if self.building_type == 'farm':
            stats['population'] = self.calc_prop('max_population')
        if self.building_type == 'castle':
            stats['building_speed_increase'] = self.calc_prop('building_speed')
        if self.building_type == 'wall':
            stats['defense_rate'] = self.calc_prop('defense_rate')

        return stats

    def to_dict(self):
        return {
            'building_type': self.building_type,
            'stage': self.stage,
            'upgrading': self.upgrading,
            'stats': self.get_stats()
        }

    def upgrade_building(self):
        self.village.update_village_resources()

        if self.upgrading:
            return code.ALREADY_UPGRADING

        b_data = settings.BUILDINGS[self.building_type]
        if b_data['max_stage'] <= self.stage:
            return code.ON_MAX_STAGE

        b_cost = b_data['building_cost']
        current_population = self.village.population()

        wood = self.calc_prop('wood', upgrade=True)
        stone = self.calc_prop('stone', upgrade=True)
        clay = self.calc_prop('clay', upgrade=True)

        if wood > self.village.wood or stone > self.village.stone or\
            clay > self.village.clay:
            return code.NOT_ENOUGH_RESOURCES

        if current_population + b_cost['population'] >\
                self.village.calc_prop('farm'):
            return code.VILLAGE_POPULATION_IS_FULL

        self.upgrading = True
        self.save()

        return UpgradeBuildingQueue.add_building(self)

    def use_resources(self):
        self.village.stone -= self.calc_prop('stone', upgrade=True)
        self.village.clay -= self.calc_prop('clay', upgrade=True)
        self.village.wood -= self.calc_prop('wood', upgrade=True)
        self.village.village_population += self.calc_prop(
            'population', upgrade=True)
        self.village.save()

    def return_resources(self):
        self.village.stone += self.calc_prop('stone', upgrade=True)
        self.village.clay += self.calc_prop('clay', upgrade=True)
        self.village.wood += self.calc_prop('wood', upgrade=True)
        self.village.village_population -= self.calc_prop(
            'population', upgrade=True)
        self.village.save()

    def calc_prop(self, prop, upgrade=False):
        bonus = 0
        factor = 'cost_factor'
        if prop is 'income_speed':
            factor = 'income_factor'
        elif prop is 'max_population':
            factor = 'population_factor'
        elif prop is 'building_speed':
            factor = 'building_factor'
        elif prop is 'storage_space':
            factor = 'storage_factor'
        elif prop is 'training_speed':
            factor = 'training_factor'
        elif prop is 'defense_rate':
            factor = 'defense_factor'
        elif prop is 'traders':
            factor = 'traders_factor'
        elif prop is 'members':
            factor = 'members_factor'

        if prop is 'time':
            try:
                castle = Building.objects.get(
                    village=self.village, building_type='castle'
                )
            except Building.DoesNotExist:
                print('castle does not exist !?!?!')

            bonus = Building.calc_property(
                'castle', castle.stage, 'building_speed', 'building_factor')
            bonus = -1 * (bonus / 100)

        if upgrade:
            return Building.calc_property(
                self.building_type, self.stage + 1, prop, factor, bonus)
        return Building.calc_property(
            self.building_type, self.stage, prop, factor, bonus)

    def start_training(self, data):
        self.village.update_village_resources()
        self.village.update_village_army_training()
        if self.building_type != 'barracks':
            return None

        def is_valid_unit(barracks, unit_type):
            sett_data = settings.BUILDINGS['barracks']['units']
            if unit_type not in sett_data:
                return False

            if sett_data[unit_type]['barrack_stage'] > barracks.stage:
                return False
            return True

        resources_needed = {
            'stone': 0,
            'wood': 0,
            'clay': 0,
            'population': 0
        }

        for unit in data:
            if not is_valid_unit(self, unit['unit_type']):
                return code.INVALID_UNIT_TYPE

            try:
                number = int(unit['number'])
            except ValueError:
                return None

            unit_data = settings.BUILDINGS['barracks']['units'][
                unit['unit_type']]['training_cost']
            resources_needed['stone'] += unit_data['stone'] * number
            resources_needed['wood'] += unit_data['wood'] * number
            resources_needed['clay'] += unit_data['clay'] * number
            resources_needed['population'] += unit_data['population'] * number

        if resources_needed['wood'] > self.village.wood or\
            resources_needed['stone'] > self.village.stone or\
                resources_needed['clay'] > self.village.clay:
            return code.NOT_ENOUGH_RESOURCES

        if resources_needed['population'] > self.village.population():
            return code.VILLAGE_POPULATION_IS_FULL

        for unit in data:
            ArmyTraining.create_training(
                self, unit['unit_type'], int(unit['number'])
            )

        return self.village.get_units_trainings()

    @staticmethod
    def create_building(village, building_type):
        village.update_village_resources()
        castle = Building.objects.get(
            village=village,
            building_type='castle'
        )

        b_data = settings.BUILDINGS[building_type]
        if castle.stage < b_data['min_castle_stage']:
            return code.CASTLE_STAGE_ERROR

        b_cost = b_data['building_cost']
        current_population = village.population()

        wood = b_cost['resources']['wood']
        stone = b_cost['resources']['stone']
        clay = b_cost['resources']['clay']

        if wood > village.wood or stone > village.stone or\
            clay > village.clay:
            return code.NOT_ENOUGH_RESOURCES

        if current_population + b_cost['population'] >\
                village.calc_prop('farm'):
            return code.VILLAGE_POPULATION_IS_FULL

        building = Building.objects.create(
            village=village,
            building_type=building_type,
            stage=0,
            upgrading=True
        )

        return UpgradeBuildingQueue.add_building(building)

    @staticmethod
    def calc_property(building, stage, prop, factor, bonus=0):
        if prop in ['stone', 'clay', 'wood', 'time']:
            val = settings.BUILDINGS[building][
                'building_cost']['resources'][prop]
        else:
            val = settings.BUILDINGS[building]['building_cost'][prop]
            if prop == 'population':
                return val

        fact = settings.BUILDINGS[building]['building_cost'][factor]
        if prop in ['building_speed', 'training_speed', 'defense_rate',
                    'members']:
            for i in range(1, stage):
                val = val + fact
        else:
            for i in range(1, stage):
                val += val * fact
        val += val * bonus

        return math.ceil(val)

    @staticmethod
    def build_default_buildings(village):
        buildings = settings.BUILDINGS
        for key in buildings.keys():
            if buildings[key]['min_stage'] > 0:
                Building.objects.create(
                    village=village,
                    building_type=key,
                    stage=buildings[key]['min_stage']
                )


class UpgradeBuildingQueue(models.Model):
    building = models.ForeignKey(Building)
    building_time = models.IntegerField(default=0)
    start_at = models.DateTimeField()
    added_at = models.DateTimeField(auto_now_add=True)

    def to_dict(self):
        return {
            'building': {
                'id': self.building.id,
                'stage': self.building.stage + 1,
                'building_type': self.building.building_type
            },
            'building_time': self.building_time,
            'start_at': int(time.mktime( self.start_at.timetuple()))
        }

    def recalculate_time(self):
        self.building_time = self.building.calc_prop('time', upgrade=True)
        self.save()

    @staticmethod
    def add_building(building):
        last_building = UpgradeBuildingQueue.objects.filter(
            building__village=building.village).order_by('-added_at').first()

        building.use_resources()

        start_at = datetime.utcnow().replace(tzinfo=timezone.utc)
        if last_building:
            start_at = last_building.start_at + timedelta(
                seconds=last_building.building_time)

        ubq = UpgradeBuildingQueue.objects.create(
            building=building,
            building_time=building.calc_prop('time', upgrade=True),
            start_at=start_at
        )

        return ubq


class ArmyTraining(models.Model):
    village = models.ForeignKey(Village, db_index=True)
    unit_type = models.CharField(max_length=20)
    number = models.IntegerField(default=0)
    training_time = models.IntegerField(default=0)

    start_at = models.DateTimeField()
    updated_time = models.DateTimeField()
    added_at = models.DateTimeField(auto_now_add=True)

    def to_dict(self):
        return {
            'village_id': self.village_id,
            'unit_type': self.unit_type,
            'training_time': self.training_time,
            'start_at': self.start_at.timestamp(),
            'number': self.not_trained()
        }

    def not_trained(self):
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        one_unit_time = self.training_time / self.number
        diff = int((self.updated_time - self.start_at).total_seconds())

        already_trained = math.floor(diff / one_unit_time)
        return self.number - already_trained


    @staticmethod
    def create_training(barracks, unit_type, number):
        barracks.village.update_village_resources()

        unit_data = settings.BUILDINGS['barracks']['units'][
            unit_type]['training_cost']

        barracks.village.stone -= unit_data['stone'] * number
        barracks.village.wood -= unit_data['wood'] * number
        barracks.village.clay -= unit_data['clay'] * number
        barracks.village.army_population += unit_data['population'] * number

        training_time = math.ceil(
            unit_data['time'] - unit_data['time'] *\
            barracks.calc_prop('training_speed') / 100) * number

        start_at = datetime.utcnow().replace(tzinfo=timezone.utc)
        armyTraining = ArmyTraining.objects.filter(
            village=barracks.village).order_by('-id').first()
        if armyTraining:
            start_at = armyTraining.start_at + timedelta(
                seconds=armyTraining.training_time)

        ArmyTraining.objects.create(
            village=barracks.village,
            unit_type=unit_type,
            number=number,
            training_time=training_time,
            updated_time=start_at,
            start_at=start_at
        )

        barracks.village.save()

class Army(object):
    units = {}
    wall_bonus = 0

    def __init__(self, army_groups):
        self.units = {}
        self.add_army_groups(army_groups)
        self.wall_bonus = 0

    def set_wall_bonus(self, wall_bonus):
        self.wall_bonus = wall_bonus

    def add_army_groups(self, army_groups):
        for ag in army_groups:
            self.add_army_group(ag)

    def add_army_group(self, army_group):
        if army_group.unit_type not in self.units:
            self.units[army_group.unit_type] = []

        exist = False
        for unit in self.units[army_group.unit_type]:
            if unit['owner_id'] == army_group.owner_village_id:
                unit['number'] += army_group.number
                exist = True

        if not exist:
            self.units[army_group.unit_type].append({
                'owner_id': army_group.owner_village_id,
                'number': army_group.number
            })

    def get_army_strength(self, strength_type='attack_power'):
        attack_power = {
            'units': 0,
            'cavalry': 0,
            'wall': 0,
            'buildings': 0,
            'scouts': 0
        }
        defense_power = {
            'units': 0,
            'cavalry': 0,
            'scouts': 0
        }

        for unit_type, units in self.units.items():
            number = 0
            for u in units:
                number += u['number']

            ap = settings.BUILDINGS['barracks']['units']\
                [unit_type]['attack_power']

            attack_power['units'] += ap['units'] * number
            attack_power['cavalry'] += ap['cavalry'] * number
            attack_power['wall'] += ap['wall'] * number
            attack_power['buildings'] += ap['buildings'] * number
            attack_power['scouts'] += ap['scouts'] * number

            dp = settings.BUILDINGS['barracks']['units']\
                [unit_type]['defense_power']

            defense_power['units'] += dp['units'] * number
            defense_power['cavalry'] += dp['cavalry'] * number
            defense_power['scouts'] += dp['scouts'] * number

        if strength_type == 'attack_power':
            return attack_power

        defense_power['units'] += math.ceil(
            (defense_power['units']/100) * self.wall_bonus)
        defense_power['cavalry'] += math.ceil(
            (defense_power['cavalry']/100) * self.wall_bonus)
        defense_power['scouts'] += math.ceil(
            (defense_power['scouts']/100) * self.wall_bonus)

        return defense_power


class Battle(object):
    attacker = None
    defender = None
    wall_bonus = None

    attacker_losses = {}
    defender_losses = {}

    def __init__(self, attacker_army_groups, defender_army_groups, wall_bonus):
        self.attacker = Army(attacker_army_groups)
        self.defender = Army(defender_army_groups)
        self.wall_bonus = wall_bonus
        self.defender.set_wall_bonus(self.wall_bonus)

        self.attacker_losses = {}
        self.defender_losses = {}

        self.battle()

    def battle(self):
        att_power = self.attacker.get_army_strength('attack_power')
        def_power = self.defender.get_army_strength('defense_power')

        self.calc_attacker_losses(def_power)
        self.calc_defender_losses(att_power)

    def add_to_attacker_losses(self, unit_type, units):
        if unit_type not in self.attacker_losses:
            self.attacker_losses[unit_type] = []

        for u in self.attacker_losses[unit_type]:
            if u['owner_id'] == units['owner_id']:
                u['number'] += units['number']
                return

        self.attacker_losses[unit_type].append(units)

    def add_to_defender_losses(self, unit_type, units):
        if unit_type not in self.defender_losses:
            self.defender_losses[unit_type] = []

        for u in self.defender_losses[unit_type]:
            if u['owner_id'] == units['owner_id']:
                u['number'] += units['number']
                return

        self.defender_losses[unit_type].append(units)

    def calc_attacker_losses(self, def_power):
        for unit_type, units_groups in self.attacker.units.items():
            for units in units_groups:
                unit_data = settings.BUILDINGS['barracks']['units'][unit_type]
                units_ap = unit_data['attack_power']['units']
                cavalry_ap = unit_data['attack_power']['cavalry']
                scouts_ap = unit_data['attack_power']['scouts']
                number = 0

                if unit_data['type'] == 'unit':
                    def_power['units'] -= units_ap * units['number']
                    if def_power['units'] > 0:
                        number = units['number']
                    else:
                        overpower = abs(def_power['units'])
                        # all - survivors
                        number = units['number'] - math.ceil(
                            overpower / units_ap)

                elif unit_data['type'] == 'cavalry':
                    def_power['cavalry'] -= cavalry_ap * units['number']
                    if def_power['cavalry'] > 0:
                        number = units['number']
                    else:
                        overpower = abs(def_power['cavalry'])
                        # all - survivors
                        number = units['number'] - math.ceil(
                            overpower / cavalry_ap)

                elif unit_data['type'] == 'scout':
                    def_power['scouts'] -= scouts_ap * units['number']
                    if def_power['scouts'] > 0:
                        number = units['number']
                    else:
                        overpower = abs(def_power['scouts'])
                        # all - survivors
                        number = units['number'] - math.ceil(
                            overpower / scouts_ap)

                self.add_to_attacker_losses(
                    unit_type, {
                        'owner_id': units['owner_id'],
                        'number': number
                    })

                if def_power['units'] < 0:
                    def_power['units'] = 0

                if def_power['cavalry'] < 0:
                    def_power['cavalry'] = 0

                if def_power['scouts'] < 0:
                    def_power['scouts'] = 0

    def calc_defender_losses(self, att_power):
        for unit_type, units_groups in self.defender.units.items():
            for units in units_groups:
                unit_data = settings.BUILDINGS['barracks']['units'][unit_type]
                units_df = unit_data['defense_power']['units']
                cavalry_df = unit_data['defense_power']['cavalry']
                scouts_df = unit_data['defense_power']['scouts']
                number = 0

                if unit_data['type'] == 'unit':
                    att_power['units'] -= units_df * units['number']
                    if att_power['units'] < 0:
                        overpower = abs(att_power['units'])
                        # all - survivors
                        number = units['number'] - math.ceil(
                            overpower / units_df)
                    else:
                        number = units['number']

                elif unit_data['type'] == 'cavalry':
                    att_power['cavalry'] -= cavalry_df * units['number']
                    if att_power['cavalry'] < 0:
                        overpower = abs(att_power['cavalry'])
                        # all - survivors
                        number = units['number'] - math.ceil(
                            overpower / cavalry_df)
                    else:
                        number = units['number']

                elif unit_data['type'] == 'scout':
                    att_power['scouts'] -= scouts_df * units['number']
                    if att_power['scouts'] < 0:
                        overpower = abs(att_power['scouts'])
                        # all - survivors
                        number = units['number'] - math.ceil(
                            overpower / scouts_df)
                    else:
                        number = units['number']

                self.add_to_defender_losses(
                    unit_type, {
                        'owner_id': units['owner_id'],
                        'number': number
                    })

                if att_power['units'] < 0:
                    att_power['units'] = 0

                if att_power['cavalry'] < 0:
                    att_power['cavalry'] = 0

                if att_power['scouts'] < 0:
                    att_power['scouts'] = 0

    def get_report(self, attacker=False):
        report = {
            'attacker': {
                'all': {},
                'killed': {},
                'success_rate': 'P'
            },
            'defender': {
                'all': {},
                'killed': {},
                'success_rate': 'P',
                'wall_bonus': None
            }
        }

        for unit_type, unit in settings.BUILDINGS['barracks']['units'].items():
            report['attacker']['all'][unit_type] = 0
            report['attacker']['killed'][unit_type] = 0
            report['defender']['all'][unit_type] = 0
            report['defender']['killed'][unit_type] = 0

        # get starting units
        for unit_type, units_arr in self.attacker.units.items():
            for units in units_arr:
                report['attacker']['all'][unit_type] += units['number']

        # get battle losses
        for unit_type, units_arr in self.attacker_losses.items():
            for units in units_arr:
                report['attacker']['killed'][unit_type] += units['number']

        # get attacker success rate and survived scouts
        survived_scouts = False
        for unit_type, unit in settings.BUILDINGS['barracks']['units'].items():
            if report['attacker']['killed'][unit_type] > 0:
                if report['attacker']['all'][unit_type] !=\
                        report['attacker']['killed'][unit_type]:
                    if unit_type == 'scout':
                        survived_scouts = True
                    report['attacker']['success_rate'] = 'C'
                elif report['attacker']['success_rate'] == 'P':
                    report['attacker']['success_rate'] = 'D'

        if attacker and report['attacker']['success_rate'] == 'D':
            return report

        # get starting units
        for unit_type, units_arr in self.defender.units.items():
            for units in units_arr:
                report['defender']['all'][unit_type] += units['number']

        # get battle losses
        for unit_type, units_arr in self.defender_losses.items():
            for units in units_arr:
                report['defender']['killed'][unit_type] += units['number']

        # get defender success rate
        for unit_type, unit in settings.BUILDINGS['barracks']['units'].items():
            if report['defender']['killed'][unit_type] > 0:
                if report['defender']['all'][unit_type] !=\
                        report['defender']['killed'][unit_type]:
                    report['defender']['success_rate'] = 'C'
                elif report['defender']['success_rate'] == 'P':
                    report['defender']['success_rate'] = 'D'

        # if there is not survived scouts hide defender army
        if attacker and not survived_scouts:
            for unit_type, unit in settings.BUILDINGS[
                    'barracks']['units'].items():
                report['defender']['all'][unit_type] = '?'

        report['defender']['wall_bonus'] = self.wall_bonus

        return report


class ArmyExpedition(models.Model):
    starting_village = models.ForeignKey(
        Village, related_name='starting_village', db_index=True)
    ending_village = models.ForeignKey(
        Village, related_name='ending_village', db_index=True)

    choices = (
        ('A', 'Attack'), ('R', 'Reinforcement'), ('B', 'Returning'),
    )
    expedition_type = models.CharField(max_length=1, choices=choices)

    wood = models.IntegerField(default=0)
    clay = models.IntegerField(default=0)
    stone = models.IntegerField(default=0)

    start_at = models.DateTimeField()
    ended_at = models.DateTimeField()

    def to_dict(self):
        return {
            'village': {
                'id' : self.starting_village_id,
                'name': self.starting_village.name,
                'owner': {
                    'id': self.starting_village.owner.id,
                    'username': self.starting_village.owner.username
                }
            },
            'target_village': {
                'id' : self.ending_village_id,
                'name': self.ending_village.name,
                'owner': {
                    'id': self.ending_village.owner.id,
                    'username': self.ending_village.owner.username
                }
            },
            'type': self.expedition_type,
            'ended_at': self.ended_at.timestamp()
        }

    def fight(self, attacker_army_groups=None):
        self.ending_village.update_village_resources()

        if attacker_army_groups is None:
            attacker_army_groups = ArmyGroup.objects.filter(
                army_expedition=self).all()

        defender_army_groups = ArmyGroup.objects.filter(
            current_village=self.ending_village
        ).all()

        # get attacked village wall
        wall_bonus = 0
        wall = self.ending_village.building_set.filter(
            building_type='wall', stage__gt=0).first()
        if wall:
            wall_stats = wall.get_stats()
            if 'defense_rate' in wall_stats:
                wall_bonus = wall_stats['defense_rate']

        battle = Battle(attacker_army_groups, defender_army_groups, wall_bonus)

        attacker_losses = battle.attacker_losses
        defender_losses = battle.defender_losses

        for unit_type, units_arr in attacker_losses.items():
            for units in units_arr:
                number = units['number']
                if number == 0:
                    continue

                for ag in attacker_army_groups:
                    if number == 0:
                        break

                    if ag.owner_village_id == units['owner_id'] and ag.unit_type == unit_type:
                        if ag.number > number:
                            killed = number
                            ag.kill_units(killed)
                            number -= killed
                            break
                        else:
                            killed = ag.number
                            ag.kill_units(killed, save_ag=False)
                            number -= killed
                            ag.delete()
                            continue

        for unit_type, units_arr in defender_losses.items():
            for units in units_arr:
                number = units['number']
                if number == 0:
                    continue

                for ag in defender_army_groups:
                    if number == 0:
                        break

                    if ag.owner_village_id == units['owner_id']:
                        if ag.number > number:
                            killed = number
                            ag.kill_units(killed)
                            number -= killed
                            break
                        else:
                            killed = ag.number
                            ag.kill_units(killed, save_ag=False)
                            number -= killed
                            ag.delete()
                            continue
        return battle

    def loot_village_resources(self, army_groups=None):
        self.ending_village.update_village_resources()

        if army_groups is None:
            army_groups = ArmyGroup.objects.filter(army_expedition=self).all()

        units = []
        carrying_capacity = 0
        for ag in army_groups:
            units.append({
                'unit_type': ag.unit_type,
                'number': ag.number
            })

            carrying_capacity += ag.calc_carrying_capacity()

        shares = split_carrying_capacity(carrying_capacity, 3)

        # steal stone
        if self.ending_village.stone >= shares[0]:
            self.ending_village.stone -= shares[0]
            self.stone = shares[0]
        else:
            self.stone = self.ending_village.stone
            self.ending_village.stone = 0
        carrying_capacity -= self.stone

        # steal wood
        shares = split_carrying_capacity(carrying_capacity, 2)
        if self.ending_village.wood >= shares[0]:
            self.ending_village.wood -= shares[0]
            self.wood = shares[0]
        else:
            self.wood = self.ending_village.wood
            self.ending_village.wood = 0
        carrying_capacity -= self.wood

        # steal clay
        if self.ending_village.clay >= carrying_capacity:
            self.ending_village.clay -= carrying_capacity
            self.clay = carrying_capacity
        else:
            self.clay = self.ending_village.clay
            self.ending_village.clay = 0

        self.ending_village.save()
        self.save()

    @staticmethod
    def create_expedition(village, target_village, units, expedition_type):
        def has_units(army_groups, unit_type, number):
            for ag in army_groups:
                print(ag.unit_type, ag.number)
                if ag.unit_type == unit_type and ag.number >= number:
                    return True
            return False

        def remove_units(army_groups, unit_type, number):
            for ag in army_groups:
                if ag.unit_type == unit_type:
                    ag.number -= int(number)
                    ag.save()

        if expedition_type not in ['A', 'R', 'B']:
            return code.INVALID_EXPEDITION_TYPE

        army_groups = ArmyGroup.objects.filter(
            current_village=village, owner_village=village
        ).all()

        units_available = True
        for u in units:
            if not has_units(army_groups, u['unit_type'], u['number']):
                return code.UNITS_NOT_AVAILABLE

        movement_speed = get_movement_speed(units)
        coordinate1 = MapCoordinate.objects.filter(village=village)\
                                   .values('x', 'y').first()
        coordinate2 = MapCoordinate.objects.filter(village=target_village)\
                                   .values('x', 'y').first()

        time = calc_travel_time(coordinate1, coordinate2, movement_speed)
        now = datetime.utcnow().replace(tzinfo=timezone.utc)

        army_expedition = ArmyExpedition.objects.create(
            starting_village=village,
            ending_village=target_village,
            expedition_type=expedition_type,
            start_at=now,
            ended_at=now + timedelta(seconds=time)
        )

        for u in units:
            remove_units(army_groups, u['unit_type'], u['number'])
            ag = ArmyGroup.objects.create(
                owner_village=village,
                current_village=None,
                army_expedition=army_expedition,
                unit_type=u['unit_type'],
                number=u['number']
            )

        return True

    @staticmethod
    def get_army_movements(village):
        army_expeditions = ArmyExpedition.objects.filter(
            (~Q(expedition_type='B') & Q(starting_village=village)) |
            Q(ending_village=village)
        )
        data = []
        for ae in army_expeditions:
            data.append(ae.to_dict())

        return data


class ArmyGroup(models.Model):
    owner_village = models.ForeignKey(
        Village, related_name='owner_village', db_index=True)
    current_village = models.ForeignKey(
        Village, null=True, blank=True, db_index=True)
    army_expedition = models.ForeignKey(
        ArmyExpedition, null=True, blank=True, db_index=True)
    unit_type = models.CharField(max_length=20)
    number = models.IntegerField(default=0)

    def calc_carrying_capacity(self):
        sett_data = settings.BUILDINGS['barracks']['units']
        cc = sett_data[self.unit_type]['carrying_capacity']

        return cc * self.number

    def kill_units(self, number, save_ag=True):
        pop = settings.BUILDINGS['barracks']['units'][self.unit_type]\
            ['training_cost']['population']
        self.owner_village.army_population -= pop * number
        self.owner_village.save()
        self.number -= number
        if save_ag:
            self.save()

    @staticmethod
    def add_to_army_groups(village, data):
        updated = []

        army_groups = ArmyGroup.objects.filter(
            owner_village=village, current_village=village,
            unit_type__in=data.keys()).all()

        for ag in army_groups:
            ag.number += data[ag.unit_type]
            updated.append(ag.unit_type)
            ag.save()

        for key in data.keys():
            if key not in updated:
                ArmyGroup.objects.create(
                    owner_village=village,
                    current_village=village,
                    unit_type=key,
                    number=data[key]
                )
                updated.append(key)

    @staticmethod
    def get_village_army(village, own):
        def add_units(data, unit_type, number):
            new = True
            for d in data:
                if d['unit_type'] == unit_type:
                    d['number'] += number
                    new = False
                    break

            if new:
                data.append({
                    'unit_type': unit_type,
                    'number': number
                })
            return data

        data = []
        query = Q(current_village=village) & Q(number__gt=0)
        if own:
            query = query & Q(owner_village=village)

        army_groups = ArmyGroup.objects.filter(
            query).values('unit_type', 'number').all()

        for ag in army_groups:
            data = add_units(data, ag['unit_type'], ag['number'])

        return data


class Report(PolymorphicModel):
    user = models.ForeignKey(User, db_index=True)
    seen = models.DateTimeField(null=True, blank=True)
    added_at = models.DateTimeField()

    def to_dict(self):
        resp = {}
        if type(self) == BattleReport:
            resp = self.to_battle_dict()
        if type(self) == MarketReport:
            resp = self.to_market_dict()
        resp['added_at'] = self.added_at.timestamp()
        resp['seen'] = self.seen

        return resp

    @staticmethod
    def get_unread_badge(user):
        return Report.objects.filter(user=user, seen__isnull=True).count()


class BattleReport(Report):
    start_village = models.ForeignKey(
        Village, related_name='report_start_village')
    end_village = models.ForeignKey(
        Village, related_name='report_end_village')
    attacker = models.BooleanField(default=True)

    stone = models.IntegerField(default=0)
    wood = models.IntegerField(default=0)
    clay = models.IntegerField(default=0)

    status = models.CharField(
        max_length=1,
        choices=(('P', 'Prefect'), ('C', 'Casualties'), ('D', 'Defeat'))
    )
    battle_meta_data = models.CharField(max_length=1000)

    def to_battle_dict(self):
        return {
            'id': self.id,
            'type': 'battle-report',
            'attacker': {
                'village': {
                    'id': self.start_village.id,
                    'name': self.start_village.name
                },
                'player': {
                    'id': self.start_village.owner.id,
                    'username': self.start_village.owner.username
                }
            },
            'defender': {
                'village': {
                    'id': self.end_village.id,
                    'name': self.end_village.name
                },
                'player': {
                    'id': self.end_village.owner.id,
                    'username': self.end_village.owner.username
                }
            },
            'resources': {
                'wood': self.wood,
                'stone': self.stone,
                'clay': self.clay
            },
            'status': self.status,
            'battle_info': json.loads(self.battle_meta_data),
            'role': 'attacker' if self.attacker else 'defender',
        }


class MarketReport(Report):
    start_village = models.ForeignKey(
        Village, related_name='market_report_start_village', db_index=True)
    end_village = models.ForeignKey(
        Village, related_name='market_report_end_village', db_index=True)

    delivered_resource = models.CharField(
        max_length=5,
        choices=(('stone', 'Stone'), ('wood', 'Wood'), ('clay', 'Clay'))
    )
    delivered_value = models.IntegerField(default=0)

    status = models.CharField(
        max_length=1,
        choices=(('D', 'Delivered'), ('R', 'Returned'))
    )

    def to_market_dict(self):
        return {
            'id': self.id,
            'type': 'market-report',
            'owner_village': {
                'id': self.start_village.id,
                'name': self.start_village.name,
                'player': {
                    'id': self.start_village.owner.id,
                    'username': self.start_village.owner.username
                }
            },
            'end_village': {
                'id': self.end_village.id,
                'name': self.end_village.name,
                'player': {
                    'id': self.end_village.owner.id,
                    'username': self.end_village.owner.username
                }
            },
            'status': self.status,
            'resource': self.delivered_resource,
            'value': self.delivered_value,
        }


class MarketOffer(models.Model):
    owner_village = models.ForeignKey(
        Village, related_name="resource_offering_village", db_index=True)
    end_village = models.ForeignKey(
        Village, related_name="resource_requesting_village", db_index=True,
        null=True, blank=True)

    choices = (('stone', 'Stone'), ('wood', 'Wood'), ('clay', 'Clay'))
    offered_resource = models.CharField(max_length=5, choices=choices)
    requested_resource = models.CharField(max_length=5, choices=choices)

    offered_value = models.IntegerField(default=0)
    requested_value = models.IntegerField(default=0)


    start_at = models.DateTimeField(null=True)
    end_at = models.DateTimeField(null=True)
    status = models.CharField(
        max_length=1, default='W',
        choices=(('W', 'Waiting'), ('D', 'Delivering'), ('R', 'Returning'))
    )

    added_at = models.DateTimeField(auto_now_add=True)

    def accept_offer(self, village):
        if self.end_village is not None:
            return {'error': code.MARKET_OFFER_ALREADY_USED}

        if self.requested_resource == 'stone' and\
                self.requested_value > village.stone:
            return {'error': code.NOT_ENOUGH_RESOURCES}
        else:
            village.stone -= self.requested_value

        if self.requested_resource == 'wood' and\
                self.requested_value > village.wood:
            return {'error': code.NOT_ENOUGH_RESOURCES}
        else:
            village.wood -= self.requested_value

        if self.requested_resource == 'clay' and\
                self.requested_value > village.clay:
            return {'error': code.NOT_ENOUGH_RESOURCES}
        else:
            village.clay -= self.requested_value

        village.save()


        movement_speed = settings.BUILDINGS['market']['movement_speed']
        coordinate1 = MapCoordinate.objects.filter(village=self.owner_village)\
                                   .values('x', 'y').first()
        coordinate2 = MapCoordinate.objects.filter(village=village)\
                                   .values('x', 'y').first()

        calc_time = calc_travel_time(coordinate1, coordinate2, movement_speed)
        self.end_village = village
        self.status = 'D'
        self.start_at = datetime.utcnow().replace(tzinfo=timezone.utc)
        self.end_at = self.start_at + timedelta(seconds=calc_time)
        self.save()

        return self.to_dict()


    def to_dict(self):
        resp = {
            'id': self.id,
            'owner_village': {
                'id': self.owner_village.id,
                'name': self.owner_village.name,
                'owner': {
                    'id': self.owner_village.owner.id,
                    'name': self.owner_village.owner.username
                }
            },
            'end_village': None,
            'offered': {
                'resource': self.offered_resource,
                'value': self.offered_value
            },
            'requested': {
                'resource': self.requested_resource,
                'value': self.requested_value
            },
            'start_at': None,
            'end_at': None,
            'status': self.status
        }

        if self.end_village:
            resp['end_village'] = {
                'id': self.end_village.id,
                'name': self.end_village.name,
                'owner': {
                    'id': self.end_village.owner.id,
                    'name': self.end_village.owner.username
                }
            }

        if self.start_at:
            resp['start_at'] = int(time.mktime(self.start_at.timetuple()))
            resp['end_at'] = int(time.mktime(self.end_at.timetuple()))
        return resp

    @staticmethod
    def get_offers(village, village_only=False):
        offers = []
        query = Q(status='W')
        if village_only:
            query = query & Q(owner_village=village)
        else:
            query = query & ~Q(owner_village=village)

        for mo in MarketOffer.objects.select_related('owner_village')\
                .filter(query).all():
            offers.append(mo.to_dict())
        return offers

    @staticmethod
    def get_num_of_used_traders(village):
        carrying_capacity = settings.BUILDINGS['market']['carrying_capacity']
        used_traders = 0

        for mo in MarketOffer.objects.filter(owner_village=village).all():
            res = mo.offered_value
            if mo.requested_value > res:
                res = mo.requested_value

            used_traders += math.ceil(res/carrying_capacity)

        return used_traders

    @staticmethod
    def create_offer(market, offered, requested):
        carrying_capacity = settings.BUILDINGS['market']['carrying_capacity']
        max_traders = market.calc_prop('traders')
        used_traders = MarketOffer.get_num_of_used_traders(market.village)
        available_traders = max_traders - used_traders

        if type(offered['value']) != int or\
                type(requested['value']) != int:
            return None

        if offered['resource'] not in ['stone', 'wood', 'clay'] or \
                requested['resource'] not in ['stone', 'wood', 'clay']:
            return None

        res = offered['value']
        if offered['value'] < requested['value']:
            res = requested['value']

        traders_needed = math.ceil(res/carrying_capacity)

        if traders_needed > available_traders:
            return None

        if offered['resource'] == 'stone':
            if market.village.stone < offered['value']:
                return None
            market.village.stone -= offered['value']
        elif offered['resource'] == 'wood':
            if market.village.wood < offered['value']:
                return None
            market.village.stone -= offered['value']
        else:
            if market.village.clay < offered['value']:
                return None
            market.village.stone -= offered['value']
        market.village.save()

        return MarketOffer.objects.create(
            owner_village=market.village,
            offered_resource=offered['resource'],
            offered_value=offered['value'],
            requested_resource=requested['resource'],
            requested_value=requested['value']
        )


class Message(models.Model):
    sender = models.ForeignKey(
        User, related_name='message_sender', db_index=True)
    receiver = models.ForeignKey(
        User, related_name='message_receiver', db_index=True)
    subject = models.CharField(max_length=255)
    content = models.TextField()

    seen = models.DateTimeField(null=True, blank=True)
    added_at = models.DateTimeField(auto_now_add=True)

    def to_dict(self):
        return {
            'id': self.id,
            'sender': {
                'id': self.sender.id,
                'username': self.sender.username
            },
            'receiver': {
                'id': self.receiver.id,
                'username': self.receiver.username
            },
            'subject': self.subject,
            'content': self.content,
            'seen': self.seen,
            'added_at': self.added_at.timestamp()
        }

    @staticmethod
    def create_message(sender, receiver, subject, content):
        return Message.objects.create(
            sender=sender,
            receiver=receiver,
            subject=subject,
            content=content
        )

    @staticmethod
    def get_unread_badge(user):
        return Message.objects.filter(receiver=user, seen__isnull=True).count()


class Clan(models.Model):
    name = models.CharField(max_length=15)
    leader = models.ForeignKey(User)
    description = models.TextField(null=True, blank=True)

    def to_dict(self, full_info=False):
        resp = {
            'id': self.id,
            'leader': {
                'id': self.leader.id,
                'username': self.leader.username
            },
            'name': self.name,
            'description': self.description
        }

        if full_info:
            resp['total_population'] = 0
            resp['members'] = []
            for member in self.get_members():
                total_population = 0
                for village in member.villages:
                    total_population += village.village_population

                resp['total_population'] += total_population
                resp['members'].append({
                    'id': member.id,
                    'username': member.username,
                    'total_population': total_population
                })

        return resp

    def get_members(self):
        members = User.objects.prefetch_related(
            Prefetch('village_set',
                     queryset=Village.objects.filter(owner__userprofile__clan=self),
                     to_attr='villages')
        ).filter(userprofile__clan=self.id).all()

        return members

    def remove_all_clan_invites(self):
        ClanInvite.objects.filter(clan=self).all().delete()


class ClanInvite(models.Model):
    user = models.ForeignKey(User)
    clan = models.ForeignKey(Clan)

    added_at = models.DateTimeField(auto_now_add=True)

    def to_dict(self):
        return {
            'id': self.id,
            'user': {
                'id': self.user.id,
                'username': self.user.username
            },
            'clan': self.clan.to_dict()
        }

    @staticmethod
    def get_invitations(user):
        inv = []
        for invite in ClanInvite.objects.select_related(
                'user', 'clan', 'clan__leader').filter(user=user):
            inv.append(invite.to_dict())

        return inv

    @staticmethod
    def get_clan_invitations(clan):
        inv = []
        for invite in ClanInvite.objects.select_related(
                'user', 'clan', 'clan__leader').filter(clan=clan):
            inv.append(invite.to_dict())

        return inv


class Forum(models.Model):
    clan = models.ForeignKey(Clan)

    def get_topics(self, user=None):
        topics = []
        f_topics = ForumTopic.objects.filter(forum=self).all()

        for topic in f_topics:
            topics.append(topic.to_dict(user))

        return topics

    @staticmethod
    def get_unread_badge(user):
        forum = user.userprofile.get_clan_forum()
        if forum is None:
            return 0

        topics = ForumTopic.objects.prefetch_related(Prefetch(
            'forumtopiclastseen_set',
            queryset=ForumTopicLastSeen.objects.filter(user=user),
            to_attr='user_last_seen'
        )).filter(forum=forum).all()

        new_posts = 0
        for topic in topics:
            if len(topic.user_last_seen) == 0:
                new_posts += TopicPost.objects.filter(
                    topic=topic).exclude(user=user).count()
            else:
                last_seen = topic.user_last_seen[0].last_seen
                new_posts += TopicPost.objects.filter(
                    added_at__gt=last_seen, topic=topic
                ).exclude(user=user).count()

        return new_posts


class ForumTopic(models.Model):
    forum = models.ForeignKey(Forum, db_index=True)
    title = models.CharField(max_length=100)

    added_at = models.DateTimeField(auto_now_add=True)

    def to_dict(self, user=None):
        resp = {
            'id': self.id,
            'title': self.title,
            'total_messages': TopicPost.objects.filter(topic=self).count(),
            'unread': 0,
            'last_seen': None
        }

        if user:
            last_seen = ForumTopicLastSeen.objects.filter(
                topic=self, user=user).first()
            if last_seen:
                resp['unread'] = TopicPost.objects.filter(
                    topic=self, added_at__gt=last_seen.last_seen
                ).exclude(user=user).count()
                resp['last_seen'] = last_seen.last_seen.replace(
                    tzinfo=timezone.utc).timestamp()
            else:
                resp['unread'] = resp['total_messages']

        return resp


class TopicPost(models.Model):
    topic = models.ForeignKey(ForumTopic, db_index=True)
    user = models.ForeignKey(User)
    content = models.TextField()

    added_at = models.DateTimeField(auto_now_add=True)

    def to_dict(self):
        return {
            'id': self.id,
            'user': {
                'id': self.user.id,
                'username': self.user.username
            },
            'content': self.content,
            'added_at': self.added_at.timestamp()
        }


class ForumTopicLastSeen(models.Model):
    topic = models.ForeignKey(ForumTopic, db_index=True)
    user = models.ForeignKey(User)

    last_seen = models.DateTimeField()
