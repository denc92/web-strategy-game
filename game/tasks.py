# -*- coding: utf-8 -*-
import json
from datetime import datetime, timedelta
from django.db.models import Q
from django.utils import timezone
from game.celery import app
from game.management.commands import generate_map_image as gmi
from game.functions import get_movement_speed, calc_travel_time

@app.task
def generate_map_image(data):
    cmd = gmi.Command()
    cmd.handle()

@app.task
def upgrade_buildings():
    from game.models import UpgradeBuildingQueue

    queue = UpgradeBuildingQueue.objects.raw(
        'SELECT * FROM game_upgradebuildingqueue '
        'WHERE DATE_ADD(start_at, INTERVAL building_time second) < NOW()')

    for q in queue:
        q.building.stage = q.building.stage + 1
        q.building.upgrading = False
        q.building.save()

        if q.building.building_type == 'castle':
            que = UpgradeBuildingQueue.objects.filter(
                building__village=q.building.village).exclude(id=q.id)
            prev = None
            for qu in que:
                if prev is None:
                    qu.start_at = datetime.utcnow().replace(
                        tzinfo=timezone.utc)
                    prev = qu
                else:
                    qu.start_at = prev.start_at + timedelta(
                        seconds=prev.building_time)
                qu.save()
                qu.recalculate_time()

            q.building.village.save()

        if q.building.building_type == 'storage':
            q.building.village.storage_space = q.building.calc_prop(
                'storage_space')
            q.building.village.save()

        print(q.building.building_type, q.building.stage)
        q.delete()


@app.task
def army_movements():
    from game.models import ArmyExpedition, ArmyGroup, MapCoordinate,\
        BattleReport

    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    army_expeditions = ArmyExpedition.objects.select_related(
        'starting_village', 'ending_village').filter(ended_at__lte=now).all()

    for army_expedition in army_expeditions:
        army_groups = ArmyGroup.objects.filter(
            army_expedition=army_expedition).all()

        if army_expedition.expedition_type == 'A':
            # create report
            battle = army_expedition.fight(army_groups)

            # get army groups after fight
            army_groups = ArmyGroup.objects.filter(
                army_expedition=army_expedition).all()

            army_expedition.loot_village_resources(army_groups)

            # Create battle report
            # attacker
            battle_report = battle.get_report(attacker=True)
            BattleReport.objects.create(
                user=army_expedition.starting_village.owner,
                start_village=army_expedition.starting_village,
                end_village=army_expedition.ending_village,
                wood=army_expedition.wood,
                clay=army_expedition.clay,
                stone=army_expedition.stone,
                battle_meta_data=json.dumps(battle_report),
                status=battle_report['attacker']['success_rate'],
                added_at=army_expedition.ended_at,
                attacker=True
            )

            # todo report for all defenders
            # defender

            battle_report = battle.get_report(attacker=False)
            BattleReport.objects.create(
                user=army_expedition.ending_village.owner,
                start_village=army_expedition.starting_village,
                end_village=army_expedition.ending_village,
                wood=army_expedition.wood,
                clay=army_expedition.clay,
                stone=army_expedition.stone,
                battle_meta_data=json.dumps(battle_report),
                status=battle_report['defender']['success_rate'],
                added_at=army_expedition.ended_at,
                attacker=False
            )

            # swap villages
            tmp = army_expedition.starting_village
            army_expedition.starting_village = army_expedition.ending_village
            army_expedition.ending_village = tmp

            units = []
            for ag in army_groups:
                units.append({
                    'unit_type': ag.unit_type,
                    'number': ag.number
                })

            movement_speed = get_movement_speed(units)
            coordinate1 = MapCoordinate.objects.filter(
                village=army_expedition.starting_village).values(
                'x', 'y').first()
            coordinate2 = MapCoordinate.objects.filter(
                village=army_expedition.ending_village).values(
                'x', 'y').first()

            time = calc_travel_time(coordinate1, coordinate2, movement_speed)
            army_expedition.start_at = army_expedition.ended_at
            army_expedition.ended_at =\
                army_expedition.start_at + timedelta(seconds=time)

            army_expedition.expedition_type = 'B'
            army_expedition.save()

        elif army_expedition.expedition_type == 'B':
            army_expedition.ending_village.update_village_resources()

            units = {}
            for ag in army_groups:
                if ag.unit_type not in units:
                    units[ag.unit_type] = 0
                units[ag.unit_type] += ag.number
                ag.delete()

            ArmyGroup.add_to_army_groups(army_expedition.ending_village, units)

            army_expedition.ending_village.stone += army_expedition.stone
            army_expedition.ending_village.wood += army_expedition.wood
            army_expedition.ending_village.clay += army_expedition.clay
            army_expedition.ending_village.save()

            army_expedition.delete()

        elif army_expedition.expedition_type == 'R':
            # TO DO:
            # create report

            for ag in army_groups:
                ag.current_village = army_expedition.ending_village
                ag.army_expedition = None
                ag.save()

            army_expedition.delete()


@app.task
def market_movement():
    from game.models import MarketOffer, MarketReport, Building

    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    marketOffers = MarketOffer.objects.select_related(
        'owner_village', 'end_village').filter(
        ~Q(status='W') & Q(end_at__lte=now)).all()

    for offer in marketOffers:
        if offer.status == 'D':
            if offer.offered_resource == 'stone':
                offer.end_village.stone += offer.offered_value
            elif offer.offered_resource == 'wood':
                offer.end_village.wood += offer.offered_value
            elif offer.offered_resource == 'clay':
                offer.end_village.clay += offer.offered_value

            if offer.end_village.stone > offer.end_village.storage_space:
                offer.end_village.stone = offer.end_village.storage_space

            if offer.end_village.wood > offer.end_village.storage_space:
                offer.end_village.wood = offer.end_village.storage_space

            if offer.end_village.clay > offer.end_village.storage_space:
                offer.end_village.clay = offer.end_village.storage_space

            offer.end_village.save()

            minutes = (offer.end_at - offer.start_at).seconds / 60
            offer.start_at = offer.end_at
            offer.end_at = offer.end_at + timedelta(minutes=minutes)
            offer.status = 'R'
            offer.save()

            # Sender
            MarketReport.objects.create(
                user=offer.owner_village.owner,
                start_village=offer.owner_village,
                end_village=offer.end_village,
                delivered_resource=offer.offered_resource,
                delivered_value=offer.offered_value,
                status='D',
                added_at=offer.end_at
            )

            # Receiver
            MarketReport.objects.create(
                user=offer.end_village.owner,
                start_village=offer.owner_village,
                end_village=offer.end_village,
                delivered_resource=offer.offered_resource,
                delivered_value=offer.offered_value,
                status='D',
                added_at=offer.end_at
            )

        else:
            if offer.requested_resource == 'stone':
                offer.owner_village.stone += offer.requested_value
            elif offer.requested_resource == 'wood':
                offer.owner_village.wood += offer.requested_value
            elif offer.requested_resource == 'clay':
                offer.owner_village.clay += offer.requested_value

            if offer.owner_village.stone > offer.owner_village.storage_space:
                offer.owner_village.stone = offer.owner_village.storage_space

            if offer.owner_village.wood > offer.owner_village.storage_space:
                offer.owner_village.wood = offer.owner_village.storage_space

            if offer.owner_village.clay > offer.owner_village.storage_space:
                offer.owner_village.clay = offer.owner_village.storage_space

            offer.owner_village.save()

            mr = MarketReport.objects.create(
                user=offer.owner_village.owner,
                start_village=offer.end_village,
                end_village=offer.owner_village,
                delivered_resource=offer.requested_resource,
                delivered_value=offer.requested_value,
                status='R',
                added_at=offer.end_at
            )

            offer.delete()
