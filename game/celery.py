from __future__ import absolute_import
import os
import sys

from game.settings.celery_config import celery_django_settings

path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.insert(0, path)
os.environ['DJANGO_SETTINGS_MODULE'] = celery_django_settings

from celery import Celery
from django.conf import settings

app = Celery('game',
             broker=settings.TASK_BROKER,
             include=['game.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)

app.config_from_object(celery_django_settings)

if __name__ == '__main__':
    app.start()
