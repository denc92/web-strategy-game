import math
from django.conf import settings


def get_movement_speed(units):
    sett_data = settings.BUILDINGS['barracks']['units']
    movement_speed = 99
    for unit in units:
        if sett_data[unit['unit_type']]['movement_speed'] < movement_speed:
            movement_speed = sett_data[unit['unit_type']]['movement_speed']

    return movement_speed


def calc_travel_time(start, end, speed):
    length = math.ceil(math.sqrt(math.pow(start['x'] - end['x'], 2)\
        + math.pow(start['y'] - end['y'], 2)))

    return math.ceil(length * (3600 / speed))

def split_carrying_capacity(number, shares=3):
    data = []
    share = math.floor(number/shares)
    leftover = number - (share * shares)

    for i in range(0, shares):
        data.append(share + leftover)
        leftover = 0

    return data

