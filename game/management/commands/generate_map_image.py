#!/usr/bin/env python
import math
from PIL import Image
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, **options):
        from game.models import MapCoordinate
        coordinates = MapCoordinate.objects.values(
            'coordinate_type', 'village').order_by('-y', 'x').all()
        line = int(math.sqrt(coordinates.count()))
        img = Image.new('RGB', (line * 5, line * 5), 'black')
        pixels = img.load()

        x = 0
        y = 0
        for coordinate in coordinates:
            color = (0,100, 0)
            if coordinate['village']:
                color = (255, 0, 0)
            # Sea
            elif coordinate['coordinate_type'] == 'S':
                color = (0, 0, 204)
            # Mountain
            elif coordinate['coordinate_type'] == 'M':
                color = (128, 128, 128)
            # Forrest
            elif coordinate['coordinate_type'] == 'F':
                color = (102, 51, 0)

            #pixels[x, y] = color
            for xx in range(x, x + 5):
                for yy in range(y, y + 5):
                    pixels[xx, yy] = color
            x = x + 5

            if x % line == 0:
                y = y + 5
                x = 0

        img.save('static/frontend/images/map-img.png')
