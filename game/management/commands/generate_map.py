#!/usr/bin/env python
import math
import random
import noise
from optparse import make_option
from django.core.management.base import BaseCommand
from django.conf import settings
from game.models import MapCoordinate


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--width', dest='width', help='--width 100'),
        make_option('--height', dest='height', help='--height 100'),
    )
    help = '--height 100 --width 100'

    def handle(self, **options):
        width = settings.DEFAULT_MAP_SETTINGS['max_x']
        height = settings.DEFAULT_MAP_SETTINGS['max_y']
        if 'width' in options and type(options['width']) == int:
            width = options['width'] / 2
        if 'height' in options and type(options['height']) == int:
            height = options['height'] / 2

        octaves = random.randrange(1, 10)
        persistence = random.uniform(0.5, 0.8)
        for x in range(-1 * width, width + 1):
            for y in range(-1 * height, height + 1):
                n = noise.snoise2(x=x, y=y, octaves=octaves,
                                  persistence=persistence)
                ct = 'S'
                if n > 0.35:
                    ct = 'M'
                elif n > 0.25:
                    ct = 'F'
                elif n > -0.4:
                    ct = 'G'

                MapCoordinate.objects.create(
                    x=x,
                    y=y,
                    coordinate_type=ct
                )
