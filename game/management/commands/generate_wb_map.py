#!/usr/bin/env python
import math
import noise
import random
from PIL import Image
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, **options):
        img = Image.new('RGB', (505, 505), 'black')
        wb_img = Image.new('RGB', (505, 505), 'black')
        pixels = img.load()
        wb_pixels = wb_img.load()

        x = 0
        y = 0
        persistence = random.uniform(0.5, 0.8)
        for x in range(0,  101):
            for y in range(0, 101):
                n = noise.snoise2(x=x, y=y, octaves=5, persistence=persistence)

                # sea
                color = (0, 0, 204)
                if n > 0.35:
                    # mountain
                    color = (128, 128, 128)
                elif n > 0.25:
                    # forest
                    color = (102, 51, 0)
                elif n > -0.4:
                    # grass
                    color = (0, 100, 0)

                value = math.ceil((n + 1) / 2 * 255)
                wb_color = (value, value, value)
                for xx in range(x * 5, x * 5 + 5):
                    for yy in range(y * 5, y * 5 + 5):
                        pixels[xx, yy] = color
                        wb_pixels[xx, yy] = wb_color

        img.save('static/frontend/images/map-rgb-img.png')
        wb_img.save('static/frontend/images/map-wb-img.png')
