# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0016_battlereport_attacker'),
    ]

    operations = [
        migrations.CreateModel(
            name='MarketOffer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('offered_resource', models.CharField(max_length=5, choices=[('stone', 'Stone'), ('wood', 'Wood'), ('clay', 'Clay')])),
                ('requested_resource', models.CharField(max_length=5, choices=[('stone', 'Stone'), ('wood', 'Wood'), ('clay', 'Clay')])),
                ('offered_value', models.IntegerField(default=0)),
                ('requested_value', models.IntegerField(default=0)),
                ('start_at', models.DateTimeField(null=True)),
                ('end_at', models.DateTimeField(null=True)),
                ('status', models.CharField(max_length=1, choices=[('W', 'Waiting'), ('D', 'Delivering'), ('R', 'Returning')])),
                ('added_at', models.DateTimeField(auto_now_add=True)),
                ('end_village', models.ForeignKey(related_name='resource_requesting_village', to='game.Village')),
                ('owner_village', models.ForeignKey(related_name='resource_offering_village', to='game.Village')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
