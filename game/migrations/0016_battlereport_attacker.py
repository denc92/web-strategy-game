# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0015_auto_20150625_1856'),
    ]

    operations = [
        migrations.AddField(
            model_name='battlereport',
            name='attacker',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
