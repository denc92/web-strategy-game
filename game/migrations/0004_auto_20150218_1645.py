# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0003_auto_20150216_1655'),
    ]

    operations = [
        migrations.RenameField(
            model_name='village',
            old_name='rock',
            new_name='stone',
        ),
    ]
