# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0007_upgradebuildingqueue'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='village',
            name='clay_income',
        ),
        migrations.RemoveField(
            model_name='village',
            name='max_clay',
        ),
        migrations.RemoveField(
            model_name='village',
            name='max_population',
        ),
        migrations.RemoveField(
            model_name='village',
            name='max_stone',
        ),
        migrations.RemoveField(
            model_name='village',
            name='max_wood',
        ),
        migrations.RemoveField(
            model_name='village',
            name='stone_income',
        ),
        migrations.RemoveField(
            model_name='village',
            name='wood_income',
        ),
    ]
