# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0010_auto_20150503_0934'),
    ]

    operations = [
        migrations.AddField(
            model_name='armytraining',
            name='updated_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 3, 9, 57, 42, 663571), auto_now_add=True),
            preserve_default=False,
        ),
    ]
