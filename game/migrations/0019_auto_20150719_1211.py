# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0018_auto_20150719_1158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketoffer',
            name='end_village',
            field=models.ForeignKey(related_name='resource_requesting_village', to='game.Village', null=True, blank=True),
            preserve_default=True,
        ),
    ]
