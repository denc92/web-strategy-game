# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0019_auto_20150719_1211'),
    ]

    operations = [
        migrations.CreateModel(
            name='MarketReport',
            fields=[
                ('report_ptr', models.OneToOneField(to='game.Report', primary_key=True, serialize=False, parent_link=True, auto_created=True)),
                ('delivered_resource', models.CharField(choices=[('stone', 'Stone'), ('wood', 'Wood'), ('clay', 'Clay')], max_length=5)),
                ('delivered_value', models.IntegerField(default=0)),
                ('status', models.CharField(default='D', choices=[('D', 'Delivered'), ('R', 'Returned')], max_length=1)),
                ('end_village', models.ForeignKey(related_name='market_report_end_village', to='game.Village')),
                ('start_village', models.ForeignKey(related_name='market_report_start_village', to='game.Village')),
            ],
            options={
                'abstract': False,
            },
            bases=('game.report',),
        ),
    ]
