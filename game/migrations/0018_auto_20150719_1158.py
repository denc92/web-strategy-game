# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0017_marketoffer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketoffer',
            name='status',
            field=models.CharField(default='W', choices=[('W', 'Waiting'), ('D', 'Delivering'), ('R', 'Returning')], max_length=1),
            preserve_default=True,
        ),
    ]
