# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0002_logintoken'),
    ]

    operations = [
        migrations.AddField(
            model_name='village',
            name='name',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='village',
            name='village_population',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
