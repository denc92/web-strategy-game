# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0022_auto_20150720_1008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='armytraining',
            name='updated_time',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
