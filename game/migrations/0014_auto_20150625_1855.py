# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0013_auto_20150615_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='added_at',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
