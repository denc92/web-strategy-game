# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0001_initial'),
        ('game', '0012_auto_20150615_1529'),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('added_at', models.DateTimeField()),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BattleReport',
            fields=[
                ('report_ptr', models.OneToOneField(to='game.Report', serialize=False, primary_key=True, auto_created=True, parent_link=True)),
                ('stone', models.IntegerField(default=0)),
                ('wood', models.IntegerField(default=0)),
                ('clay', models.IntegerField(default=0)),
                ('status', models.CharField(max_length=1, choices=[('P', 'Prefect'), ('C', 'Casualties'), ('D', 'Defeat')])),
                ('battle_meta_data', models.CharField(max_length=1000)),
                ('end_village', models.ForeignKey(related_name='report_end_village', to='game.Village')),
                ('start_village', models.ForeignKey(related_name='report_start_village', to='game.Village')),
            ],
            options={
                'abstract': False,
            },
            bases=('game.report',),
        ),
        migrations.AddField(
            model_name='report',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, related_name='polymorphic_game.report_set+', to='contenttypes.ContentType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='report',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
