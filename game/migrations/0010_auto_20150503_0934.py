# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0009_auto_20150502_0759'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArmyExpedition',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('expedition_type', models.CharField(max_length=1, choices=[('A', 'Attack'), ('R', 'Reinforcement'), ('B', 'Returning')])),
                ('wood', models.IntegerField(default=0)),
                ('clay', models.IntegerField(default=0)),
                ('stone', models.IntegerField(default=0)),
                ('start_at', models.DateTimeField(auto_now_add=True)),
                ('ended_at', models.DateTimeField(auto_now_add=True)),
                ('ending_village', models.ForeignKey(related_name='ending_village', to='game.Village')),
                ('starting_village', models.ForeignKey(related_name='starting_village', to='game.Village')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ArmyGroup',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('unit_type', models.CharField(max_length=20)),
                ('number', models.IntegerField(default=0)),
                ('army_expedition', models.ForeignKey(to='game.ArmyExpedition', null=True, blank=True)),
                ('current_village', models.ForeignKey(to='game.Village', null=True, blank=True)),
                ('owner_village', models.ForeignKey(related_name='owner_village', to='game.Village')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ArmyTraining',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('unit_type', models.CharField(max_length=20)),
                ('number', models.IntegerField(default=0)),
                ('training_time', models.IntegerField(default=0)),
                ('start_at', models.DateTimeField(auto_now_add=True)),
                ('added_at', models.DateTimeField(auto_now_add=True)),
                ('village', models.ForeignKey(to='game.Village')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='building',
            name='building_type',
            field=models.CharField(max_length=20, default='castle', choices=[('castle', 'Castle'), ('stone_mine', 'Stone mine'), ('clay_pit', 'Clay pit'), ('lumber_camp', 'Lumber camp'), ('farm', 'Farm'), ('barracks', 'Barracks')], db_index=True),
            preserve_default=True,
        ),
    ]
