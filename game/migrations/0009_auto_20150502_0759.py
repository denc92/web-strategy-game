# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0008_auto_20150308_2128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='building_type',
            field=models.CharField(default='castle', choices=[('castle', 'Castle'), ('stone_mine', 'Stone mine'), ('clay_pit', 'Clay pit'), ('lumber_camp', 'Lumber camp'), ('farm', 'Farm'), ('barracks', 'Barracks')], max_length=20),
            preserve_default=True,
        ),
    ]
