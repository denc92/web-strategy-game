# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0005_auto_20150218_1659'),
    ]

    operations = [
        migrations.AddField(
            model_name='building',
            name='building_type',
            field=models.CharField(default='castle', max_length=20, choices=[('castle', 'Castle'), ('stone_mine', 'Stone mine'), ('clay_pit', 'Clay pit'), ('lumber_camp', 'Lumber camp'), ('farm', 'Farm')]),
            preserve_default=True,
        ),
    ]
