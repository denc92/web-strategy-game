# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0020_marketreport'),
    ]

    operations = [
        migrations.AddField(
            model_name='village',
            name='storage_space',
            field=models.IntegerField(default=1000),
            preserve_default=True,
        ),
    ]
