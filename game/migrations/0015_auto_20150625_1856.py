# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0014_auto_20150625_1855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='added_at',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
