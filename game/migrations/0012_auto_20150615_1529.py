# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0011_armytraining_updated_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='armyexpedition',
            name='ended_at',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='armyexpedition',
            name='start_at',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='armytraining',
            name='start_at',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='upgradebuildingqueue',
            name='start_at',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
