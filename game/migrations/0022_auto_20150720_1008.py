# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0021_village_storage_space'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketreport',
            name='status',
            field=models.CharField(max_length=1, choices=[('D', 'Delivered'), ('R', 'Returned')]),
            preserve_default=True,
        ),
    ]
