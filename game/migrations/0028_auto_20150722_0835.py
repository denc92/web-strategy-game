# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('game', '0027_auto_20150722_0719'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForumTopic',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('added_at', models.DateTimeField(auto_now_add=True)),
                ('forum', models.ForeignKey(to='game.Forum')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ForumTopicLastSeen',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('last_seen', models.DateTimeField()),
                ('topic', models.ForeignKey(to='game.ForumTopic')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TopicPost',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField()),
                ('added_at', models.DateTimeField(auto_now_add=True)),
                ('topic', models.ForeignKey(to='game.ForumTopic')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='forumpost',
            name='topic',
        ),
        migrations.RemoveField(
            model_name='forumpost',
            name='user',
        ),
        migrations.DeleteModel(
            name='ForumPost',
        ),
        migrations.RemoveField(
            model_name='topic',
            name='forum',
        ),
        migrations.RemoveField(
            model_name='topiclastseen',
            name='topic',
        ),
        migrations.DeleteModel(
            name='Topic',
        ),
        migrations.RemoveField(
            model_name='topiclastseen',
            name='user',
        ),
        migrations.DeleteModel(
            name='TopicLastSeen',
        ),
    ]
