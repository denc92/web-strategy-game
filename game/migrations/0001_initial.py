# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('stage', models.SmallIntegerField(default=0)),
                ('upgrading', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MapCoordinate',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('x', models.SmallIntegerField()),
                ('y', models.SmallIntegerField()),
                ('coordinate_type', models.CharField(default='G', choices=[('G', 'Grass'), ('S', 'Sea'), ('F', 'Forest'), ('M', 'Mountain')], max_length=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Village',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('rock', models.IntegerField(default=0)),
                ('wood', models.IntegerField(default=0)),
                ('clay', models.IntegerField(default=0)),
                ('max_population', models.IntegerField(default=0)),
                ('loyalty', models.SmallIntegerField(default=100)),
                ('last_updated', models.DateTimeField(auto_now_add=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='mapcoordinate',
            name='village',
            field=models.ForeignKey(null=True, to='game.Village', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='building',
            name='village',
            field=models.ForeignKey(to='game.Village'),
            preserve_default=True,
        ),
    ]
