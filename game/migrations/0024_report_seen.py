# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0023_auto_20150720_1208'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='seen',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
