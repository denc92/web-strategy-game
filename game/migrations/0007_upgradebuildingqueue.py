# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0006_building_building_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='UpgradeBuildingQueue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('building_time', models.IntegerField(default=0)),
                ('start_at', models.DateTimeField(auto_now_add=True)),
                ('added_at', models.DateTimeField(auto_now_add=True)),
                ('building', models.ForeignKey(to='game.Building')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
