from game.settings.base import *

INTERNAL_IPS = ['192.168.50.1']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'localhost',
        'NAME': 'game',
        'USER': 'root',
        'PASSWORD': 'password',
        'PORT': '3306',
    }
}

INSTALLED_APPS += (
    'debug_toolbar',
    'debug_panel',
)

MIDDLEWARE_CLASSES += (
    'debug_panel.middleware.DebugPanelMiddleware',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    },

    # this cache backend will be used by django-debug-panel
    'debug-panel': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/debug-panel-cache',
        'OPTIONS': {
            'MAX_ENTRIES': 200
        }
    }
}
