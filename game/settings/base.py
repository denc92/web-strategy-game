"""
Django settings for game project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import djcelery
djcelery.setup_loader()
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '1eo)6tb!ee3#=^u67vv^s17s^t%%m6_*8yml&#qbnp97-9m*@$'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ["192.168.50.240:8000", "192.168.50.240"]


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'game',
     # CORS (Cross-Origin Resource Sharing) headers
    'corsheaders',
    'djcelery',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'game.urls'

WSGI_APPLICATION = 'game.wsgi_dev.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(os.path.dirname(BASE_DIR), 'static'),
)

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(BASE_DIR), 'templates'),
)

# celery queues setup
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
TASK_BROKER = 'amqp://guest@localhost//'
CELERY_ROUTES = {
    'game.tasks.generate_map_image': {
        'queue': 'default',
        'routing_key': 'default'
    },
}

CELERYBEAT_SCHEDULE = {
    'buildings-upgrade': {
        'task': 'game.tasks.upgrade_buildings',
        'schedule': 1
    },
    'army-movement': {
        'task': 'game.tasks.army_movements',
        'schedule': 1
    },
    'market_movement': {
        'task': 'game.tasks.market_movement',
        'schedule': 1
    }
}

CELERY_TIMEZONE = 'UTC'

# GAME variables

REPORTS = {
    'page_size': 10
}

MESSAGES = {
    'page_size': 10
}

FORUM = {
    'post_per_page': 10
}

DEFAULT_MAP_SETTINGS = {
    'max_x': 50,
    'min_x': -50,
    'max_y': 50,
    'min_y': -50,
    'screen_size': 15
}

DEFAULT_VILLAGE_SETTINGS = {
    'resources': 800,
    'max_resources': 1000,
    'village_population': 10,
    'max_population': 20,
    'income': 10,
}

BUILDINGS = {
    'castle': {
        'name': 'Castle',
        'min_stage': 1,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 70,
                'wood': 70,
                'clay': 70,
                'time': 50
            },
            'population': 2,
            'cost_factor': 0.7,
            'building_factor': 2,
            'building_speed': 2
        }
    },
    'stone_mine': {
        'name': 'Stone mine',
        'min_castle_stage': 1,
        'min_stage': 1,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 10,
                'wood': 60,
                'clay': 50,
                'time': 40
            },
            'population': 2,
            'cost_factor': 0.6,
            'income_factor': 0.5,
            'income_speed': 10
        }
    },
    'clay_pit': {
        'name': 'Clay pit',
        'min_castle_stage': 1,
        'min_stage': 1,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 60,
                'wood': 50,
                'clay': 10,
                'time': 40
            },
            'population': 2,
            'cost_factor': 0.6,
            'income_factor': 0.5,
            'income_speed': 10
        }
    },
    'lumber_camp': {
        'name': 'Lumber camp',
        'min_castle_stage': 1,
        'min_stage': 1,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 50,
                'wood': 10,
                'clay': 60,
                'time': 40
            },
            'population': 2,
            'cost_factor': 0.6,
            'income_factor': 0.5,
            'income_speed': 10
        }
    },
    'farm': {
        'name': 'Farm',
        'min_castle_stage': 1,
        'min_stage': 1,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 50,
                'wood': 60,
                'clay': 50,
                'time': 40
            },
            'population': 0,
            'cost_factor': 0.6,
            'population_factor': 0.6,
            'max_population': 25
        }
    },
    'storage': {
        'name': 'Storage',
        'min_castle_stage': 1,
        'min_stage': 1,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 80,
                'wood': 80,
                'clay': 80,
                'time': 70
            },
            'population': 2,
            'cost_factor': 0.6,
            'storage_factor': 0.5,
            'storage_space': 1000
        }
    },
    'market': {
        'name': 'Market',
        'min_castle_stage': 3,
        'min_stage': 0,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 100,
                'wood': 120,
                'clay': 90,
                'time': 80
            },
            'population': 2,
            'cost_factor': 0.8,
            'traders_factor': 1,
            'traders': 1
        },
        'carrying_capacity': 500,
        'movement_speed': 10
    },
    'embassy': {
        'name': 'Embassy',
        'min_castle_stage': 5,
        'min_stage': 0,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 120,
                'wood': 120,
                'clay': 120,
                'time': 120
            },
            'population': 2,
            'cost_factor': 0.8,
            'members_factor': 3,
            'members': 3
        }
    },
    'wall': {
        'name': 'Wall',
        'min_castle_stage': 3,
        'min_stage': 0,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 80,
                'wood': 80,
                'clay': 80,
                'time': 70
            },
            'population': 1,
            'cost_factor': 0.8,
            'defense_factor': 2,
            'defense_rate': 2
        }
    },
    'barracks': {
        'name': 'Barrack',
        'min_castle_stage': 3,
        'min_stage': 0,
        'max_stage': 10,
        'building_cost': {
            'resources': {
                'stone': 120,
                'wood': 120,
                'clay': 120,
                'time': 200
            },
            'population': 4,
            'cost_factor': 1,
            'training_factor': 2,
            'training_speed': 2
        },
        'units': {
            'swordsman': {
                'name': 'Swordsman',
                'barrack_stage': 1,
                'training_cost': {
                    'stone': 40,
                    'wood': 60,
                    'clay': 70,
                    'population': 1,
                    'time': 900
                },
                'attack_power': {
                    'units': 30,
                    'cavalry': 20,
                    'wall': 0,
                    'buildings': 0,
                    'scouts': 0
                },
                'defense_power': {
                  'units': 45,
                  'cavalry': 15,
                  'scouts': 0
                },
                'type': 'unit',
                'carrying_capacity': 70,
                'movement_speed': 7
            },
            'pikemen': {
                'name': 'Pikemen',
                'barrack_stage': 3,
                'training_cost': {
                    'stone': 70,
                    'wood': 100,
                    'clay': 90,
                    'population': 1,
                    'time': 1200
                },
                'attack_power': {
                    'units': 25,
                    'cavalry': 45,
                    'wall': 0,
                    'buildings': 0,
                    'scouts': 0
                },
                'defense_power': {
                  'units': 15,
                  'cavalry': 45,
                  'scouts': 0
                },
                'type': 'unit',
                'carrying_capacity': 50,
                'movement_speed': 7
            },
            'axeman': {
                'name': 'Axeman',
                'barrack_stage': 5,
                'training_cost': {
                    'stone': 100,
                    'wood': 120,
                    'clay': 130,
                    'population': 1,
                    'time': 1600
                },
                'attack_power': {
                    'units': 75,
                    'cavalry': 15,
                    'wall': 0,
                    'buildings': 0,
                    'scouts': 0
                },
                'defense_power': {
                  'units': 50,
                  'cavalry': 25,
                  'scouts': 0
                },
                'type': 'unit',
                'carrying_capacity': 30,
                'movement_speed': 9
            },
            'scout': {
                'name': 'Scout',
                'barrack_stage': 4,
                'training_cost': {
                    'stone': 120,
                    'wood': 150,
                    'clay': 120,
                    'population': 3,
                    'time': 1800
                },
                'attack_power': {
                    'units': 0,
                    'cavalry': 0,
                    'wall': 0,
                    'buildings': 0,
                    'scouts': 10
                },
                'defense_power': {
                  'units': 0,
                  'cavalry': 0,
                  'scouts': 20
                },
                'type': 'scout',
                'carrying_capacity': 0,
                'movement_speed': 20
            },
            'light_cavalry': {
                'name': 'Light Cavalry',
                'barrack_stage': 6,
                'training_cost': {
                    'stone': 200,
                    'wood': 250,
                    'clay': 220,
                    'population': 3,
                    'time': 1800
                },
                'attack_power': {
                    'units': 150,
                    'cavalry': 110,
                    'wall': 0,
                    'buildings': 0,
                    'scouts': 0
                },
                'defense_power': {
                  'units': 100,
                  'cavalry': 100,
                  'scouts': 0
                },
                'type': 'cavalry',
                'carrying_capacity': 150,
                'movement_speed': 15
            },
            'heavy_cavalry': {
                'name': 'Heavy Cavalry',
                'barrack_stage': 8,
                'training_cost': {
                    'stone': 300,
                    'wood': 350,
                    'clay': 320,
                    'population': 5,
                    'time': 2700
                },
                'attack_power': {
                    'units': 200,
                    'cavalry': 170,
                    'wall': 0,
                    'buildings': 0,
                    'scouts': 0
                },
                'defense_power': {
                  'units': 130,
                  'cavalry': 130,
                  'scouts': 0
                },
                'type': 'cavalry',
                'carrying_capacity': 100,
                'movement_speed': 15
            }
        }
    }
}
